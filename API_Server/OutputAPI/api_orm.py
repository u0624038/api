#coding:utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, Index,VARCHAR,BOOLEAN,VARBINARY,Float,TEXT
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import create_engine,and_, or_
from sqlalchemy.pool import NullPool
import os
import shutil
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
user = config["DB"]["user"]
password = config["DB"]["password"]
host = config["DB"]["host"]
MDB_name = config["DB"]["MDB_name"]

DBInfo = "mysql+pymysql://"+ user +":"+password+ "@" + host+ "/" + MDB_name+"?charset=utf8mb4"
#DBlink = create_engine(DBInfo, encoding='utf-8', pool_timeout=20, pool_recycle=299,pool_size=100)
DBlink = create_engine(DBInfo,poolclass=NullPool)
Base = declarative_base()

class User(Base):
    __tablename__ = 'User'
    UserID = Column(Integer, primary_key=True)
class Profile(Base):
    __tablename__ = 'Profile'
    ProfileID = Column(VARCHAR(60), primary_key=True)
    Name = Column(VARCHAR(20))
    Gender = Column(VARCHAR(10))
    Age = Column(Integer)
    Others = Column(TEXT)
    UserID = Column(Integer)
class Vendor(Base):
    __tablename__ = 'Vendor'
    VendorID = Column(Integer, primary_key=True)
    VendorName = Column(VARCHAR(30))
class Interview(Base):
    __tablename__ = 'Interview'
    InterviewID = Column(Integer, primary_key=True)
    GenerateStatus = Column(BOOLEAN)
    EmotionStatus = Column(Integer)
    AudioStatus = Column(Integer)
    IssueStatus = Column(Integer)
    OfficeID = Column(Integer)
    ProfileID = Column(VARCHAR(60))
    VendorID = Column(Integer)
class Item(Base):
    __tablename__ = 'Item'
    ItemID = Column(Integer, primary_key=True)
    InterviewID = Column(Integer)
class Question(Base):
    __tablename__ = 'Question'
    #ID = Column(Integer, primary_key=True)
    QuestionID=Column(Integer)
    Language = Column(VARCHAR(20))
    Time = Column(Integer)
    AudioURL = Column(VARCHAR(50))
    ItemID = Column(Integer, primary_key=True)
class Session(Base):
    __tablename__ = 'Session'
    SessionID = Column(VARCHAR(50), primary_key=True)
    AVPath = Column(VARCHAR(60))
    VTTURL = Column(VARCHAR(60))
    VideoURL = Column(VARCHAR(60))
    ItemID = Column(Integer)
    InterviewID = Column(Integer)
class Job(Base):
    __tablename__ = 'Job'
    JobID = Column(Integer,primary_key=True)
    MetaData = Column(VARCHAR(120))
    Path = Column(VARCHAR(60))
class GenerateProcess(Base):
    __tablename__ = 'GenerateProcess'
    ProcessID = Column(Integer,primary_key=True)
    Status = Column(VARCHAR(20))
    Killed = Column(Integer)
    InterviewID = Column(Integer)
class EmotionProcess(Base):
    __tablename__ = 'EmotionProcess'
    ProcessID = Column(Integer,primary_key=True)
    Status = Column(VARCHAR(20))
    Killed = Column(Integer)
    SessionID = Column(VARCHAR(20))
class AudioProcess(Base):
    __tablename__ = 'AudioProcess'
    ProcessID = Column(Integer,primary_key=True)
    Status = Column(VARCHAR(20))
    Killed = Column(Integer)
    SessionID = Column(VARCHAR(20))
class NaNaProcess(Base):
    __tablename__ = 'NaNaProcess'
    ProcessID = Column(Integer,primary_key=True)
    Status = Column(VARCHAR(20))
    Killed = Column(Integer)
    SessionID = Column(VARCHAR(50))
class ProcessCheck(Base):
    __tablename__ = 'ProcessCheck'
    ID = Column(Integer,primary_key=True)
    PID = Column(Integer)
    Host = Column(VARCHAR(20))
    Agent = Column(VARCHAR(20))
    Time = Column(Integer)
    SessionID = Column(VARCHAR(50))
    IntegerID = Column(VARCHAR(20))
class EmotionResult(Base):
    __tablename__ = 'EmotionResult'
    ResultID = Column(Integer,primary_key=True)
    EmotionInfo = Column(VARCHAR(300))
    SessionID = Column(VARCHAR(20),index=True)
class AudioResult(Base):
    __tablename__ = 'AudioResult'
    ResultID = Column(Integer,primary_key=True)
    AnswerWords = Column(VARCHAR(1000))
    SessionID = Column(VARCHAR(50),index=True)
class TestResult(Base):
    __tablename__ = 'TestResult'
    ResultID = Column(Integer,primary_key=True)
    ValueID = Column(Integer)
    InterestID = Column(Integer) 
    SessionID = Column(VARCHAR(50))
def init_db():
    Base.metadata.create_all(DBlink)

def drop_db():
    Base.metadata.drop_all(DBlink)
#drop_db()
init_db()
