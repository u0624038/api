#coding:utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, UniqueConstraint, Index # 元素/主key
from sqlalchemy import Integer, String, VARCHAR, TEXT
from sqlalchemy.orm import sessionmaker, relationship, backref # 創接口/建立關系relationship(table.ID)
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool# sqlalchemy 查詢前連結，结束後，調用 session.close() 關閉連結
from sqlalchemy import desc #降序
import os, shutil
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
user = config["DB"]["user"]
password = config["DB"]["password"]
host = config["DB"]["host"]
QDB_name = config["DB"]["QDB_name"]

q_DBInfo = "mysql+pymysql://"+ user +":"+password+ "@" + host+ "/" + QDB_name+"?charset=utf8mb4"
q_DBLink = create_engine(q_DBInfo, poolclass=NullPool)# 創建一個空的資料庫
Base = declarative_base()# 創數據結構放資料

class PositionQuestion(Base):
    __tablename__ = "PositionQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    QuestionType = Column(TEXT)
    SuggestAnswer = Column(TEXT, nullable=False)
    Tag = Column(TEXT)
    SP = Column(TEXT)
    Class = Column(TEXT)
    PositionID = Column(VARCHAR(6))

    def __init__(self, QuestionID, QuestionNum, Question, QuestionType, PositionID, Class, Tag, SP, **kwargs):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.QuestionType = QuestionType
        self.PositionID = PositionID
        self.Class = Class
        self.Tag = Tag
        self.SP = SP
        self.SuggestAnswer = kwargs["SuggestAnswer"] if "SuggestAnswer" in kwargs else None
# PositionID	QuestionType	SP	Tag	Class	Question	SuggestAnswer


class GeneralQuestion(Base):
    __tablename__ = "GeneralQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    SuggestAnswer = Column(TEXT, nullable=False)
    Tag = Column(TEXT)
    Class = Column(TEXT)  

    def __init__(self, QuestionID, QuestionNum, Question, Class, Tag, **kwargs):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.Class = Class
        self.Tag = Tag
        self.SuggestAnswer = kwargs["SuggestAnswer"] if "SuggestAnswer" in kwargs else None
    

# class Industry(Base):
#     __tablename__ = "Industry"
#     IndustryID = Column(VARCHAR(6), primary_key=True)
#     Industry = Column(VARCHAR(50))

#     def __init__(self, IndustryID, Industry):
#         self.IndustryID = IndustryID
#         self.Industry = Industry
        

class Position(Base):
    __tablename__ = "Position"
    PositionID = Column(VARCHAR(6), primary_key=True)
    Position = Column(VARCHAR(50))

    def __init__(self, PositionID, Position):
        self.PositionID = PositionID
        self.Position = Position
        

class Tag(Base):
    __tablename__ = "Tag"
    TagID = Column(Integer, primary_key=True)
    Tag = Column(VARCHAR(4))
    SuggestAnswer = Column(TEXT)

    def __init__(self, TagID, Tag, SuggestAnswer):
        self.TagID = TagID
        self.Tag = Tag
        self.SuggestAnswer = SuggestAnswer

#冠穎新增的
class FollowQuestion(Base):
    __tablename__ = "FollowQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    DeleteStatus = Column(VARCHAR(1), nullable=False, default="0")
    SuggestAnswer = Column(TEXT, nullable=False)
    QTag = Column(TEXT) #標籤
    MainQuestionNum = Column(TEXT) #主題目的QuestionNum
    VoicePath = Column(TEXT)
    VoiceStatus = Column(TEXT, nullable=False, default="0")
    def __init__(self, QuestionID, QuestionNum, Question, MainQuestionNum, SuggestAnswer,QTag,VoiceStatus):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.QTag = QTag
        self.MainQuestionNum = MainQuestionNum
        self.SuggestAnswer = SuggestAnswer
        # self.VoicePath = VoicePath
        self.VoiceStatus = VoiceStatus

        
# class R_Position_Question_Tag(Base):
#     __tablename__ = "R_Position_Question_Tag"
#     QuestionID = Column(
#         Integer, ForeignKey("PositionQuestion.QuestionID"), primary_key=True)
#     TagID = Column(
#         Integer, ForeignKey("Tag.TagID"),  primary_key=True)

#     QuestionID = relationship(
#         "PositionQuestion", backref=backref("R_Position_Question_Tag"))
#     TagID = relationship(
#         "Tag", backref=backref("R_Position_Question_Tag"))


# class R_General_Question_Tag(Base):
#     __tablename__ = "R_General_Question_Tag"
#     QuestionID = Column(
#         Integer, ForeignKey("GeneralQuestion.QuestionID"), primary_key=True)
#     TagID = Column(
#         Integer, ForeignKey("Tag.TagID"),  primary_key=True)

#     QuestionID = relationship(
#         "GeneralQuestion", backref=backref("R_Position_Question_Tag"))
#     TagID = relationship(
#         "Tag", backref=backref("R_Position_Question_Tag"))


def init_db():
    Base.metadata.create_all(q_DBLink)


def drop_db():
    Base.metadata.drop_all(q_DBLink)

init_db()
