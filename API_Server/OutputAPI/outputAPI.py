# -*- coding: utf-8 -*-
from flask import Flask, jsonify,request
from api_orm import *
from Question_orm import *
import json
from flask_cors import CORS
import logging
import socket
import time
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
ca = config["SSL"]["ca"]
private = config["SSL"]["private"]
port = config["SSL"]["port"]
host = config["SSL"]["host"]

app = Flask(__name__)
CORS(app)
Session_ = sessionmaker(bind=DBlink)
session = Session_()
q_Session_ = sessionmaker(bind=q_DBLink)
q_session = q_Session_()

logging.basicConfig(filename='outputAPI.log', level=logging.INFO)

class AdvancedJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, '__jsonencode__'):
            return obj.__jsonencode__()
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


def udpsplunk(msg):
    UDP_IP = "163.18.26.108"
    UDP_PORT = 7602
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(json.dumps(msg), "utf-8"), (UDP_IP, UDP_PORT))

#總�??��?計�?(CEmotion_dict = Completely_Emotion_dict)
def CompletelyEmotion(CEmotion_dict, emotion_dict):
    print(type(CEmotion_dict))
    print(type(emotion_dict))
    if type(emotion_dict)== str:
        emo = {'EmotionType': '0.0', 'angry': '0.0', 'disgust': '0.0', 'fear': '0.0', 'happy': '0.0', 'sad': '0.0', 'surprise': '0.0', 'neutral': '0.0'}
        print(type(emo))
        return emo 
    for key,value in emotion_dict.items():
        if key == 'EmotionType':
            continue
        t_value = float(CEmotion_dict[key])
        CEmotion_dict[key] = t_value+float(value)
    return CEmotion_dict


#?��?類別?�斷
def EmotionLogic(emotion):
    all = 0.0
    if type(emotion)== str:
        return 0
    for key,value in emotion.items():
      all += float(value)
    h = float(emotion['happy'])
    n = float(emotion['neutral'])
    foo = h + n
    if foo < 0.3 and all >= 0.7:
        return 2
    elif foo >= 0.45 and all >= 0.7:
        return 1
    elif 0.3 <= foo <0.45 and all >= 0.7:
        return 0
    elif all <= 0.7:
        return 3

@app.route('/v2/Result/CheckInterviewProcess', methods=['POST'])
def CheckInterviewProcess():
    session = Session_()
    output = []
    data = request.get_json()
    print(data)
    list = data['InterviewList']
    for id in list:
        try:
            foo = session.query(Interview).filter(Interview.InterviewID == id).first()
            all_AVPath = session.query(Session, Session.ItemID).filter(Session.InterviewID == id, Session.AVPath != '').all()
            #print(fooi)
            #print(foo.EmotionStatus, len(all_AVPath))
           # if foo.EmotionStatus >= len(all_AVPath) and foo.EmotionStatus != 0:
            if foo.EmotionStatus >= 10 and foo.EmotionStatus != 0:
              IID_ok = foo.InterviewID
              output.append(IID_ok)
              print(IID_ok, len(all_AVPath))
            session.commit()
        except:
            session.rollback()
            pass
        finally:
            session.close()
    json_output = {'InterviewList':output}
    print(json_output)
    msg = {"APIName":"CheckInterviewProcess", 
            "ServerIP":"163.18.2.36",
            "Request_IP": str(request.remote_addr), 
            "Request_Data": data,
            "Return_Data": json_output,
            "server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
    udpsplunk(msg)
    return jsonify(json_output)


@app.route('/v2/Result/GetInterviewResult', methods=['POST'])
def GetInterviewResult():
    session = Session_()
    q_session = q_Session_()
    i = 0
    data = request.get_json()
    id = data['InterviewID']
    error = 1
    Sessionlist = []
    SessionCount = 0
    CEmotion_dict = {'EmotionType': 0.0, 'angry': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happy': 0.0, 'sad': 0.0, 'surprise': 0.0, 'neutral': 0.0}
    all_AVPath = session.query(Session, Session.ItemID).filter(Session.InterviewID == id, Session.AVPath != '').order_by(Session.ItemID).all()
    
    for row, item_id in all_AVPath:
        session.close()
        i = i+1
        S_id = row.SessionID
        #?��?題庫DB
        foo = session.query(Question.QuestionID).filter(Question.ItemID == item_id).first()
        print(foo)
        session.close()
        if foo == None:
          continue
        for bar in foo:
          Q_id = bar
        try:
            if Q_id < 1000:
                bar = q_session.query(GeneralQuestion).filter(GeneralQuestion.QuestionID == Q_id).first()
                session.close()
                SessionContent = bar.Question
                #print(SessionContent.decode(encoding='utf-8'))
                Suggestion = bar.SuggestAnswer
                #print(Suggestion)
                #if Suggestion == "":
                #Suggestion = q_session.query(GeneralTag.SuggestAnswer).join(R_General_Question_Tag).filter(R_General_Question_Tag.QuestionID == Q_id).first()
            elif Q_id > 1000 and Q_id < 100000:
                Q_id = Q_id - 1000
                bar = q_session.query(PositionQuestion).filter(PositionQuestion.QuestionID == Q_id).first()
                SessionContent = bar.Question
                #print(SessionContent)
                Suggestion = bar.SuggestAnswer
                #print(Suggestion)
                if Suggestion == "":
                    Q_tag = bar.Tag
                    PQ_Tag = q_session.query(Tag).filter(Tag.Tag == Q_tag).first()
                    Suggestion = PQ_Tag.SuggestAnswer
            elif Q_id > 200000:
                Q_id = Q_id - 200000
                bar = q_session.query(FollowQuestion).filter(FollowQuestion.QuestionID == Q_id).first()
                SessionContent = bar.Question
                #print(SessionContent)
                Suggestion = bar.SuggestAnswer
                #print(Suggestion)
                if Suggestion == "":
                    Q_tag = bar.QTag
                    PQ_Tag = q_session.query(Tag).filter(Tag.Tag == Q_tag).first()
                    Suggestion = PQ_Tag.SuggestAnswer
        except Exception as ex:
            print("QID ERROR")
            msg = {"APIName":"GetInterviewResult",
                "ServerIP":"163.18.2.36" ,
                "Request_IP": str(request.remote_addr), 
                "Request_Data": data,
                "Status":"QIDError",
                "ErrorType":ex,
                "Server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
            udpsplunk(msg)
            continue 
            #print(Suggestion)
        VideoURL = row.VideoURL
        try:
            AnswerWords = session.query(AudioResult.AnswerWords).filter(AudioResult.SessionID == S_id, AudioResult.AnswerWords != '').first()[0]
            session.close()
        except:
            print("AudioResult NOT FIND -------------------")
            AnswerWords ='Null'
        try:
          foo = session.query(EmotionResult.EmotionInfo).filter(EmotionResult.SessionID == S_id, EmotionResult.EmotionInfo != '').first()
          for bar in foo:
            print(bar)
            S_emotion = eval(bar)
        except Exception as ex:
            S_emotion = {'EmotionType': 0.0, 'angry': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happy': 0.0, 'sad': 0.0, 'surprise': 0.0, 'neutral': 0.0}
            print("EmotionResult NOT FIND ------------------")
            print(ex)
            error = 0
            msg = {"APIName":"GetInterviewResult",
                "ServerIP":"163.18.2.36" ,
                "Request_IP": str(request.remote_addr), 
                "Request_Data": data,
                "Status":"Error",
                "ErrorType":ex,
                "Server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
            udpsplunk(msg)
        if type(S_emotion)==str:
            S_emotion = {'EmotionType': 0.0, 'angry': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happy': 0.0, 'sad': 0.0, 'surprise': 0.0, 'neutral': 0.0}
        CEmotion_dict = CompletelyEmotion(CEmotion_dict,S_emotion)
        print("funtion finished")
        S_emotionType = EmotionLogic(S_emotion)
        S_emotion.update({'EmotionType':S_emotionType})
        foo = {'SessionID': S_id, 'SessionContent': SessionContent, 'VideoURL': VideoURL, 'AnswerWords': AnswerWords, 'Suggestion': Suggestion, 'SessionEmotion': S_emotion}
        #foo = {'SessionID': 'S_id', 'SessionContent': 'SessionContent', 'VideoURL': 'VideoURL', 'AnswerWords': 'AnswerWords', 'Suggestion': 'Suggestion', 'SessionEmotion': 'S_emotion'}
        print('message is ok')
        if error == 0:
          foo['SessionEmotion'] = 'Unprocessed'
        Sessionlist.append(foo)
        SessionCount += 1
    print("Question finished")
    print(type(CEmotion_dict))
    print(CEmotion_dict)
    if SessionCount == 0:
        Interview = {
        'InterviewID' : id,
        'SessionCount' : SessionCount,
        'CompletelyEmotion': CEmotion_dict,
        'Session' : Sessionlist
        }
        session.close()
        q_session.close()
        msg = {"APIName":"GetInterviewResult",
            "ServerIP":"163.18.2.36" ,
            "Request_IP": str(request.remote_addr), 
            "Request_Data": data,
            "Return_Data": Interview,
            "Status":"NoData",
            "Server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
        udpsplunk(msg)
        return json.dumps(Interview)
    for key, value in CEmotion_dict.items():
       print(key,value)
       CEmotion_dict[key] = value/SessionCount
    CEmotion_dict['EmotionType'] = EmotionLogic(CEmotion_dict)
    Interview = {
        'InterviewID' : id,
        'SessionCount' : SessionCount,
        'CompletelyEmotion': CEmotion_dict,
        'Session' : Sessionlist
    }
    json_output = json.dumps(Interview,ensure_ascii=False)
    for k in CEmotion_dict:
        CEmotion_dict[k] = 0.0
    session.close()
    q_session.close()
    msg = {"APIName":"GetInterviewResult",
            "ServerIP":"163.18.2.36" ,
            "Request_IP": str(request.remote_addr), 
            "Request_Data": data,
            "Return_Data": json_output,
            "Status":"GetData",
            "Server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
    udpsplunk(msg)
    return json_output

@app.route("/Alive/outputAPI", methods=["POST"])
def Alive():
    data = {"ServerName": "outputAPI", "Status": "ok"}
    print(data)
    return data


if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=2019)
    app.run(host=host, port=port, debug=False, ssl_context=(ca, private)) 
