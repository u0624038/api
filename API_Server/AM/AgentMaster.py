# coding=UTF-8
from flask import Flask, session, redirect, url_for,request
import time,json,os
import subprocess
import threading
import requests
import socket
import psutil
import datetime
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
import datetime
import time
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
host = config["AM"]["host"]

app = Flask(__name__)
#-------------Call Agents-----------------
def main():
    #-------- Agents name-----------------
    headers = {'Content-Type': 'application/json'}
    #Agents=['GenerateProcess','NaNaProcess','AudioProcess']
    #Routes=['SearchGenerateProcess','SearchNaNaProcess','SearchAudioProcess']
    #Port=['1825','4003','4003']
    

    Agents=['NaNaProcess','AudioProcess']
    Routes=['SearchNaNaProcess','SearchAudioProcess']
    Port=['4003','4003']
    #Agents=['GenerateProcess']
    #Routes=['SearchGenerateProcess']
    #Port=['1825']

    while 1:#1=true
        time.sleep(2)
        for i in range(0,len(Agents)):
            time.sleep(1)
            try:
                r = requests.post('http://localhost:4003/v2/Session/'+Routes[i],headers=headers)
                #print(r.text)
                if r.text !="None":
                    data = json.loads(r.text)
                    if Agents[i]=='GenerateProcess':
                        InterviewID=str(data["InterviewID"])
                        SessionID=''
                    else:
                        SessionID = str(data["SessionID"])
                        InterviewID='0'
                    time.sleep(2)
                    if Agents[i]=='UploadProcess':
                        Proc = subprocess.Popen(['sudo','python3','UploadProcess.py','--SessionID='+SessionID,'--InterviewID='+InterviewID, '--file=./files/'+SessionID+".mkv",'--title='+SessionID])
                    elif Agents[i]=='GenerateProcess':
                        a={
                        'InterviewID':InterviewID,
                        'OfficeID':data["OfficeID"],
                        'SessionID':SessionID,
                        'Profile':data["Profile"]
                        }
			
                        b=json.dumps(a)
                        print(type(b))
                        Proc = subprocess.Popen(['sudo','python3','GenerateProcess.py',b])
                    else:
                        Proc = subprocess.Popen(['sudo','python3',Agents[i]+'.py',SessionID,InterviewID])
                    print('--------------DO : '+Agents[i]+'-----------------')
                    PID =Proc.pid #拿到呼叫起來的副程式PID proc.pid 進程ID
                    #將PID Post到API 存到資料庫 (確認副程式存活狀況用)
                    Time_ =time.strftime('%I%M%S',time.localtime(time.time())) #%I 12小時制 M分鐘 S秒數
                    if Agents[i]=='GenerateProcess':
                        info={"SessionID":SessionID,"InterviewID":InterviewID,"Time":Time_,"PID":PID,"Host":"0.0.0.0","Agent":Agents[i]}
                    else:
                        info={"SessionID":SessionID,"InterviewID":InterviewID,"Time":Time_,"PID":PID,"Host":host,"Agent":Agents[i]}
                    r_init=requests.post('http://localhost:4003/v2/Process/InitProcess',data=json.dumps(info),headers=headers)#post到api 存狀況進入ProcessCheck這張表
                    print(r_init.text)
                    foo={"Agent":Agents[i],"Status":"Processing","SessionID":SessionID,"InterviewID":InterviewID}
                    r_up=requests.post('http://localhost:'+Port[i]+"/v2/Process/Update"+Agents[i],data=json.dumps(foo),headers=headers)
                else:#等於none的話就是處理好了 所以pass
                    print('Pass')
                    pass
            except Exception as e:
                
                print("AMerror:",e)
                pass
#--------------Check Timeout-----------------
#每十秒確認跟APIserver 程式存活狀況
def CheckAlive ():
    headers = {'Content-Type': 'application/json'}
    while 1:
        time.sleep(30)
        #print('try')
        print(psutil.cpu_times())
        oneday = datetime.timedelta(days=60)
        start2date = datetime.datetime.now().day
        start2month =  datetime.datetime.now().month
        print((starttime+oneday).month)
        #print(start2date)
        try:
            #POST API 確認副程式存活
            a={'Host':host}
            r_cp=requests.post('http://localhost:4003/v2/Process/CheckSubProcess',data=json.dumps(a),headers=headers)
            #print("RRRRR:",r_cp.text)
            if r_cp.text !="None":#不等於None的話會回傳pid和time
                data=json.loads(r_cp.text)
                #print('data',data)
                #抓當前時間
                now_time=time.strftime('%I%M%S',time.localtime(time.time()))
                now_time=int(now_time)
                #拿資料庫拿到的時間
                time_=int(data["Time"])
                #相減>100 的話把副程式殺掉 (拿PID殺)
                if (now_time-time_) >100:
                    PID=str(data["PID"])
                    #linux kill -9
                    #winodws taskill
                    os.system("sudo kill -9"+PID)# 殺程序只能用pid來殺
                    foo = {"PID":PID,"Host":host}
                    #print("Post")
                    r_kp=requests.post('http://localhost:4003/v2/Process/KillProcess',data=json.dumps(foo),headers=headers)
                    #print("Text:",r_kp.text)
                    #post api去把pid從表內刪除
                else :#相減時間<=100
                    pass
            else:#r_cp.text==None
                pass
        except Exception as e:
            
            print("KillError",e)
            pass
        if start2date == startdate and (starttime+oneday).month==start2month:
            sender = 'F108118121@nkust.edu.tw'
            passwd = 'ulqmbnwmmxswropk'
            receivers = ['F108118121@nkust.edu.tw']
            emails = [elem.strip().split(',') for elem in receivers]
            msg = MIMEMultipart()
            msg['Subject'] = "SSL"
            msg['From'] = sender
            msg['To'] = ','.join(receivers)
            part = MIMEText("SSL Expiry")
            msg.attach(part)
            smtp = smtplib.SMTP("smtp.gmail.com:587")
            smtp.ehlo()
            smtp.starttls()
            smtp.login(sender, passwd)
            smtp.sendmail(msg['From'], emails , msg.as_string())
            print('Send mails to',msg['To'])
        else:
            print('today %d %d'%(start2month,start2date))
#------------------------Beacon---------------------------
#接收呼叫起來的副程式每10秒傳Keepalive過來  NaNa.py呼叫beacon.py beacon.py再post到這 確認nana是否存活
@app.route('/Beacon',methods=['POST'])
def Beacon():
    headers = {'Content-Type': 'application/json'}
    Data_Dict = json.loads(bytes.decode(request.data))
    Agent = Data_Dict["Agent"]
    Status = Data_Dict["Status"]
    Time_ = Data_Dict["Time"]
    SessionID = Data_Dict["SessionID"]
    InterviewID = Data_Dict["InterviewID"]
    data={
    "Agent":Agent,
    "Status":Status,
    "Time":Time_,
    "SessionID":SessionID,
    "InterviewID":InterviewID
    }
    r_up_bc = requests.post('http://localhost:4003/v2/Process/UpdateProcess',data=json.dumps(data),headers=headers)#post更新進程狀態
    return(json.dumps(Data_Dict))
if __name__ == '__main__':
    #-----------------------
    Call_Agent_Thread=threading.Thread(target=main)
    Call_Agent_Thread.daemon = True
    Call_Agent_Thread.start()
    #-----------------------
    Check_Agent_Thread= threading.Thread(target=CheckAlive)
    Check_Agent_Thread.daemon = True
    Check_Agent_Thread.start()
    #-----------------------
    starttime = datetime.datetime.now()
    startdate = datetime.datetime.now().day
    print(starttime)
    app.run(host='localhost',port=5000)
