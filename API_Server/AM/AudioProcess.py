import speech_recognition
import ffmpy
import sys
import requests,json
from beacon import *
import threading
import time
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
#ffmpeg -i stereo.flac -ac 1 mono.flac
#-------------Post Beacon-----------
def KeepAlive(Agent,Status,SessionID):
    global k
    while k:
        time.sleep(20)
        beacon(Agent,Status,SessionID,0)
def updateStatus(Agent,Status,SessionID):
    headers = {'Content-Type': 'application/json'}
    ip=config["AudioAgent"]["host"]
    port=config["AudioAgent"]["port"]
    router='v2/Process/UpdateAudioProcess'
    foo={"Agent":Agent,"Status":Status,"SessionID":SessionID}
    requests_updater_status=requests.post('http://'+ip+':'+port+'/'+router,data=json.dumps(foo),headers=headers)
def UpdateResult(Result,SessionID):
    headers = {'Content-Type': 'application/json'}
    ip=config["AudioAgent"]["host"]
    port=config["AudioAgent"]["port"]
    router='v2/Process/UpdateAudioAIResult'
    foo={"Words":Result,"SessionID":SessionID}
    requests_updater_result=requests.post('http://'+ip+':'+port+'/'+router,headers=headers,data=json.dumps(foo))
def Speech_Recogniztion(FilesName):
    try:
        FilePaht="./files/"
        r = speech_recognition.Recognizer()
        with speech_recognition.AudioFile(FilePaht+FilesName+".flac") as source:
            audio = r.record(source)
        return (r.recognize_google(audio,language='zh-tw'))
    except Exception as e:
        print("AudioError::",e)
        return "Null"
def main(SessionID):
    global k
    #語音辨識
    UpdateResult(Speech_Recogniztion(SessionID),SessionID)
    time.sleep(4)
    updateStatus("AudioProcess","Processed",SessionID)
    #呼叫beacon post agentMaster 的Beacon (更新存活狀況)
    beacon("AudioProcess","Processed",SessionID,0)
    k=0
if __name__ == '__main__':
    global k
    k=1
    SessionID=sys.argv[1]
    t=threading.Thread(target=KeepAlive,args=('AudioProcess','Processing',SessionID))
    t.daemon = True
    t.start()
    main(SessionID)
