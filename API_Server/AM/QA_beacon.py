# coding=UTF-8
import time,os,sys,json,requests
def beacon(Agent,Status,SessionID,InterviewID): #在NaNa.py 被呼叫
    Beacon={
    "Agent":Agent,
    "Status":Status,
    "Time":time.strftime('%I%M%S',time.localtime(time.time())),
    "SessionID":SessionID,
    "InterviewID":int(InterviewID)
    }
    Beacon_json=json.dumps(Beacon) #轉成str傳出json格式
    r = requests.post('http://localhost:4500/Beacon',data=Beacon_json) #post agentMaster.py
