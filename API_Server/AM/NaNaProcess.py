# coding=UTF-8
import time,os,sys,json,requests,threading
from beacon import *
import ffmpy
import configparser

config = configparser.ConfigParser()
config.read('set.conf')


#-------------Post Beacon-----------
def KeepAlive(Agent,Status,SessionID):
    global k
    while k:
        time.sleep(20)
        beacon(Agent,Status,SessionID,0)
#----------Upload Emotion Server----
def upload(FileName):
    #將處理好的影片上傳到另一個處理情緒的Server 做處理
    ip=config["NaNaAgent"]["EmotionServer_host"]
    port=config["NaNaAgent"]["EmotionServer_port"]
    router='UploadFiles'
    Files = {'File': open('./files/'+str(FileName)+'.mkv', 'rb')}
    r_file = requests.post('http://'+ip+':'+port+'/'+router, files=Files)
#----------Video Cutter-------------
def cutter(FileName):
    #-------------把影片(MP4)的聲音單獨切出來(flac 單聲道)---------------------
    ff = ffmpy.FFmpeg(
    inputs={'./files/'+str(FileName)+'.mkv':'-y' },
    outputs={'./files/'+str(FileName)+'.flac':'-ac 1 -c:v copy'}
    )
    #-i stereo.flac -ac 1 mono.flac
    ff.run()
def updateStatus(Agent,Status,SessionID):
    headers = {'Content-Type': 'application/json'}
    ip= config["NaNaAgent"]["host"]
    port= config["NaNaAgent"]["port"]
    router='v2/Process/UpdateNaNaProcess'
    foo={"Agent":Agent,"Status":Status,"SessionID":SessionID}
    r=requests.post('http://'+ip+':'+port+'/'+router,headers=headers,data=json.dumps(foo))
    print("Update",r.text,str(foo))
#----------Agent主程式--------------
def main(SessionID):
    global k
    #影音分割
    upload(SessionID)
    try:
        cutter(SessionID)
    except Exception as e:
        print("cutter error:",e)
    #上傳檔案到情緒辨識的Server
    #upload(SessionID)
    print('sleep')
    time.sleep(4)
    updateStatus("NaNaProcess","Processed",SessionID)
    print('NaNaProcessed1')
    #呼叫beacon post agentMaster 的Beacon (更新存活狀況)
    beacon("NaNaProcess","Processed",SessionID,0)
    print('NaNaProcessed1')
    #停調Beacon的Thread
    k=0
if __name__ == '__main__':
    global k
    k=1
    SessionID=sys.argv[1]
    t=threading.Thread(target=KeepAlive,args=('NaNaProcess','Processing',SessionID))
    t.daemon = True
    t.start()
    main(SessionID)
