#coding:utf-8
import sys
import requests
import json
import QA_beacon
import threading
import time
import Question_orm
import random

Session = Question_orm.sessionmaker(bind=Question_orm.DBLink)
session = Session()


def main(SessionID, InterviewID, OfficeID, worktime):
    QuestionCount = 5
    PQ_Proportion = 0.75
    Result = GenerateProcess(OfficeID, QuestionCount, worktime, PQ_Proportion)
    print(Result)
    UpdateResult(InterviewID, Result)
    #time.sleep(4)
    UpdateStatus("GeneratePrcoess","Processed",InterviewID)
    QA_beacon.beacon("GenerateProcess", "Processed", SessionID, InterviewID)
    k = 0
    return None


def UpdateResult(InterviewID, QIDs):
    ip = "127.0.0.1"
    port = "1825"
    router = "v2/Process/GenerateQuestion"
    foo = {
        "InterviewID": InterviewID, 
        "Language": ["zh_tw"] * len(QIDs), 
        "QuestionID": QIDs, 
        "VideoPath": [""] * len(QIDs), 
        "Time": [120] * len(QIDs)
        }
    r = requests.post("http://" + ip + ":" + port + "/" + router, data=json.dumps(foo))
    return None


def UpdateStatus(Agent, Status, InterviewID):
    ip = "127.0.0.1"
    port = "1825"
    router = "v2/Process/UpdateGenerateProcess"
    foo = {"Agent": Agent, "Status": Status, "InterviewID": InterviewID}
    r = requests.post("http://" + ip + ":" + port + "/" + router, data=json.dumps(foo))
    return None


def GenerateProcess(PositionID, QuestionCount, worktime, PQ_Proportion):
    QuestionCount -= 1 # 為人格特質
    print(PositionID)
    Positionclass = ["通用", "有工作經驗", "無工作經驗", "新鮮人"]
    OPositionQuestion = session.query(Question_orm.PositionQuestion).\
        filter(Question_orm.PositionQuestion.PositionID == PositionID).\
        filter(Question_orm.PositionQuestion.QuestionType == "職務性").all()
    # GeneralQuestion = session.query(Question_orm.GeneralQuestion).all()
    # 辦斷工作年份
    if worktime == 0:
        totalPositionQuestion = []
        for QC in OPositionQuestion:
            if QC.Class == Positionclass[0] or QC.Class == Positionclass[2] or QC.Class == Positionclass[3]:
                totalPositionQuestion.append(QC)
                print("nnnnnnn")
    elif worktime > 0:
        totalPositionQuestion = []
        for QC in OPositionQuestion:
            if QC.Class == Positionclass[0] or QC.Class == Positionclass[1]:
                totalPositionQuestion.append(QC)
                print("eeeeeee") 
    totalPositionQuestionCount = len(totalPositionQuestion)  # 共幾個問題(職業)
    print(totalPositionQuestionCount)
    QIDs = []
    if totalPositionQuestionCount < int(QuestionCount * PQ_Proportion) + 1:  # 職務題比例
        for PQ in totalPositionQuestion:
            PQid = int(PQ.QuestionID)+1000 
            QIDs.append(PQid)   # 當題目數不夠
            print(PQ.QuestionID)
           # print("AA")
    else:
        for i in range(int(QuestionCount * PQ_Proportion)): 
            QID = PQadd(PositionID, worktime)+1000  # 產生職務題
            while QID in QIDs:  # 防問題相同
                QID = PQadd(PositionID, worktime)+1000  # 產生職務題
            QIDs.append(QID)
            for pqq in totalPositionQuestion:  # 列出題目測試
                if pqq.QuestionID == QID-1000:
                    print(pqq.QuestionID)
                    #print("bb")
    GQNumber = QuestionCount - len(QIDs)
    GQID = GQadd(GQNumber, worktime)
    QIDs.extend(GQID)  # 加入通用題
    # for i in range(QuestionCount - len(QIDs)):  # 剩下的問題
    #     QID = GQadd()  # 產生共通題
    #     while QID in QIDs:  # 防問題相同
    #         QID = GQadd()  # 產生共通題
    #     QIDs.append(QID)
    #     for gqq in totalGeneralQuestion:
    #         if gqq.QuestionID == QID:
    #         QID = GQadd()  # 產生共通題
    #     QIDs.append(QID)
    #     for gqq in totalGeneralQuestion:
    #         if gqq.QuestionID == QID:
    #             print(gqq.QuestionID, gqq.Question, gqq.Class)
    random.shuffle(QIDs)  # 洗一洗
   #  print(QIDs)
    QIDs.insert(0, 0) # 人格特質
    return QIDs


def PQadd(PositionID, worktime):
    Positionclass = ["通用", "有工作經驗", "無工作經驗", "新鮮人"]
    OPositionQuestion = session.query(Question_orm.PositionQuestion).\
        filter(Question_orm.PositionQuestion.PositionID == PositionID).\
        filter(Question_orm.PositionQuestion.QuestionType == "職務性").all()
    # GeneralQuestion = session.query(Question_orm.GeneralQuestion).all()
    # 辦斷工作年份
    if worktime == 0:
        totalPositionQuestion = []
        for QC in OPositionQuestion:
            if QC.Class == Positionclass[0] or QC.Class == Positionclass[2] or QC.Class == Positionclass[3]:
                totalPositionQuestion.append(QC)
    elif worktime > 0:
        totalPositionQuestion = []
        for QC in OPositionQuestion:
            if QC.Class == Positionclass[0] or QC.Class == Positionclass[1]:
                totalPositionQuestion.append(QC)
    QIDs = []
    # data = {"知識": 5, "技能": 5, "特質": 3, "動機": 3, "性格": 2, "自我概念": 2, "工作經驗": 6}
    # tag = {"知識": 0, "技能": 0, "特質": 0, "動機": 0, "性格": 0, "自我概念": 0, "工作經驗": 0}  # (動態)
    # tag = {}  # 存入目前題目有的tag(固定)
    sp = {"外顯": 0, "內隱": 0}
    for PQSP in totalPositionQuestion:
        for spName in sp.keys():
            if PQSP.SP == spName:
                sp[PQSP.SP] += 1
    # print(sp)
    QstNCount = 0
    for QstN in totalPositionQuestion:  # 辦斷建議是否為空
        if (QstN.SP == "外顯" or QstN.SP == '內隱') and QstN.SuggestAnswer != "":
            QstNCount += 1
    
    if QstNCount >= 6:  # 建議題目的數量
        for PQ in totalPositionQuestion:    
            if (PQ.SP == "外顯" or PQ.SP == '內隱') and PQ.SuggestAnswer != "":
                QIDs.append(PQ.QuestionID)
    else:
        for PQ in totalPositionQuestion:    
            if PQ.SP == "外顯" or PQ.SP == '內隱':
                QIDs.append(PQ.QuestionID)
    # if sp["外顯"] != 0 and sp["內隱"] != 0:   
        # if sp["專業"] > sp["情境"]:
        #     for PQSP in totalPositionQuestion:    
        #         if PQSP.SP == "情境":
        #             QIDs.append(PQSP.QuestionID)
        #     cksp = False
        #     PQID = random.choice(QIDs)
        #     return PQID
        # elif sp["情境"] > sp["專業"]:
        #     for PQSP in totalPositionQuestion:    
        #         if PQSP.SP == "專業":
        #             QIDs.append(PQSP.QuestionID)
        #     cksp = False
        #     PQID = random.choice(QIDs)
        #     return PQID

    # for PQ in totalPositionQuestion:  # 固定比例
    #     for ck in data.keys():
    #         if PQ.Tag == ck:
    #             tag.setdefault(PQ.Tag, data[PQ.Tag])

    # for PQ in totalPositionQuestion:  # 動態
    #     for ck in data.keys():
    #         if PQ.Tag == ck:
    #             tag[PQ.Tag] += data[PQ.Tag]
    # print(tag)
    # total = sum(tag.values())
    # rad = random.randint(1, total)
    # cur_total = 0
    # PQtag = ""
    # for k, v in tag.items():  # 用權重取出tag
    #     cur_total += v
    #     if rad <= cur_total:
    #         PQtag = k 
    #         break
    
    # for FPQ in totalPositionQuestion:
    #     if FPQ.Tag == PQtag:
    #         QIDs.append(FPQ.QuestionID)
    PQID = random.choice(QIDs)
    return PQID
    
    
def GQadd(GQNumber, worktime):
    Generalclass = ["通用", "有工作經驗", "無工作經驗"]
    # totalGeneralQuestion = session.query(Question_orm.GeneralQuestion).all()# 拿一般的問題
    OGeneralQuestion = session.query(Question_orm.GeneralQuestion).all()  # 取工作經驗(通用)
    
    if worktime == 0:
        totalGeneralQuestion = []
        for QC in OGeneralQuestion:
            if QC.Class == Generalclass[0] or QC.Class == Generalclass[2]:
                totalGeneralQuestion.append(QC)
                #print(QC.QuestionID)
    elif worktime > 0:
        totalGeneralQuestion = []
        for QC in OGeneralQuestion:
            if QC.Class == Generalclass[0] or QC.Class == Generalclass[1]:
                totalGeneralQuestion.append(QC)
                #print(QC.QuestionID)
    totalGeneralQuestionCount = len(totalGeneralQuestion)  # 一般問題的數量
    TT = []
    for QT in totalGeneralQuestion:
        TT.append(QT.Tag)
    Tag_num = len(list(set(TT)))
    # print(Tag_num)
    GQID = []
    GTag = []
    for i in range(GQNumber):
        randomN = random.randint(0, totalGeneralQuestionCount - 1)
        ID = totalGeneralQuestion[randomN].QuestionID
        Tag = totalGeneralQuestion[randomN].Tag
        if GQNumber < 6:
            while Tag in GTag:  # 防TAG相同
                randomN = random.randint(0, totalGeneralQuestionCount - 1)
                ID = totalGeneralQuestion[randomN].QuestionID
                Tag = totalGeneralQuestion[randomN].Tag
            while ID in GQID:  # 防問題相同
                randomN = random.randint(0, totalGeneralQuestionCount - 1)
                ID = totalGeneralQuestion[randomN].QuestionID
                Tag = totalGeneralQuestion[randomN].Tag  # 隨機放入一般的問題
        else:
            while ID in GQID or ID == 0:  
                randomN = random.randint(0, totalGeneralQuestionCount - 1)
                ID = totalGeneralQuestion[randomN].QuestionID
                Tag = totalGeneralQuestion[randomN].Tag
        for GQ in totalGeneralQuestion:
            if GQ.QuestionID == ID:
                print(GQ.QuestionID)
                GTag.append(Tag)
                GQID.append(ID)
    return GQID


def KeepAlive(Agent, Status, SessionID, InterviewID):
    global k
    while k:
        time.sleep(10)
        QA_beacon.beacon(Agent, Status, SessionID, InterviewID)


if __name__ == "__main__":
    global k
    k = 1
    inputData = json.loads(sys.argv[1])
    sessionID = inputData["SessionID"]
    interviewID = int(inputData["InterviewID"])
    officeID = int(inputData["OfficeID"])
    #worktime = 0
    try:
        workfile = inputData["Profile"]
        #print(workfile)
        worktime = workfile['Years of Working Experience']
        print(worktime)
        #print(type(workfile))
        #print(workfile)
        #worktime = workfile['Profile']['Years of Working Experience']
        #print(worktime)
        #print(type(worktime))
    except:
        print("worktimeERROR")
        worktime = 0
    main(sessionID, interviewID, officeID, worktime)
