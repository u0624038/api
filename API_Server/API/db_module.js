var Sequelize = require('sequelize');
const bodyParser = require('body-parser');

//var sequelize = new Sequelize('node_mysql', 'honyi', 'honyi_1013', {
//    host: '163.18.26.127',
//    dialect: 'mysql'
//});

var sequelize = new Sequelize('i2aFor1111_Test', 'root', 'c217db', {
  host: '163.18.26.141',
  dialect: 'mysql',
  pool: {
    max: 150,
    min: 0,
    acquire:90000,
    idle: 10000
  }
});

var sequelize_QA = new Sequelize('i2aFor1111_Question', 'root', 'c217db', {
  host: '163.18.26.141',
  dialect: 'mysql',
  pool:{
    max:100,
    min:0,
    acquire:90000,
    idle: 10000
  }
});

var Question_view = sequelize_QA.define('QuestionContentView', {
  QID: {type:Sequelize.BIGINT,primaryKey: true,},
  Content:{type:Sequelize.TEXT,},
  VoicePath:{type:Sequelize.TEXT,}
  
},{timestamps:false,freezeTableName: true});


var Job = sequelize.define('Job', {
  JobID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  MetaData: {type: Sequelize.STRING, defaultValue: ''},
  Path: {type:Sequelize.STRING, defaultValue: ''},
  
},{timestamps: true,freezeTableName: true});

var Vendor = sequelize.define('Vendor', {
  VendorID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true},
  VendorName: {type: Sequelize.STRING},
  
},{timestamps: true,freezeTableName: true});

var Item = sequelize.define('Item', {
  ItemID: {type:Sequelize.INTEGER,primaryKey: true},
  InterviewID: {type:Sequelize.INTEGER,allowNull: false }

},{timestamps: true,freezeTableName: true,});

var Session = sequelize.define('Session', {
  SessionID: {type:Sequelize.STRING,primaryKey: true,allowNull: false},
  AVPath:{type:Sequelize.STRING, defaultValue: ''},
  VTTURL:{type:Sequelize.STRING, defaultValue: ''},
  VideoURL:{type:Sequelize.STRING, defaultValue: ''},
  ItemID:{type:Sequelize.INTEGER},
  InterviewID:Sequelize.INTEGER,
  Orientation:{type:Sequelize.INTEGER, defaultValue :0},
},{timestamps: true,freezeTableName: true,});


var GenerateProcess = sequelize.define('GenerateProcess', {
  ProcessID: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false, autoIncrement: true,},
  InterviewID:{type:Sequelize.INTEGER, unique: true, allowNull: false, },
  Status:{type:Sequelize.STRING, defaultValue: 'unprocess'},
  Killed:{type:Sequelize.INTEGER,defaultValue: 0 },
  
},{timestamps: true,freezeTableName: true,});

var Interview = sequelize.define('Interview', {
  InterviewID: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false, autoIncrement: true,},
  GenerateStatus:{type:Sequelize.BOOLEAN, defaultValue: 0, },
  EmotionStatus:{type:Sequelize.INTEGER, },
  AudioStatus:{type:Sequelize.INTEGER, },
  IssueStatus:{type:Sequelize.INTEGER, },
  OfficeID:{type:Sequelize.INTEGER, },
  ProfileID:{type:Sequelize.STRING, },
  VendorID:{type:Sequelize.INTEGER, },
  
},{timestamps: true,freezeTableName: true,});

var User = sequelize.define('User', {
  UserID: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false, autoIncrement: true,},

},{timestamps: true,freezeTableName: true,});

var UserProfile = sequelize.define('UserProfile', {
  ProfileID: {type:Sequelize.STRING,primaryKey: true, allowNull: false, autoIncrement: true,},
  Name: {type:Sequelize.STRING,},
  Gender: {type:Sequelize.STRING,},
  Age: {type:Sequelize.INTEGER,},
  Others: {type:Sequelize.TEXT,},
  UserID: {type:Sequelize.INTEGER,},

},{timestamps: false,freezeTableName: true,});

var Question = sequelize.define('Question', {
  //ID: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false, autoIncrement: true,},
  QuestionID: {type:Sequelize.INTEGER,},
  Language: {type:Sequelize.STRING,},
  Time: {type:Sequelize.INTEGER,},
 // Type: {type:Sequelize.JSON,},
  ItemID: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false},
  AudioURL: {type:Sequelize.STRING,},

},{timestamps: true,freezeTableName: true,});

var ProcessCheck = sequelize.define('ProcessCheck', {
  ID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  Time: {type:Sequelize.INTEGER,},
  SessionID: {type:Sequelize.STRING,},
  InterviewID: {type:Sequelize.INTEGER,},
  PID: {type:Sequelize.INTEGER,},
  Host: {type:Sequelize.STRING,},
  Agent: {type:Sequelize.STRING,},
},{timestamps:true,freezeTableName: true,});

var NanaProcess = sequelize.define('NaNaProcess', {
  ProcessID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  Status: {type:Sequelize.STRING, defaultValue: 'unprocess'},
  Killed: {type:Sequelize.INTEGER, defaultValue: 0},
  
},{timestamps: true,freezeTableName: true,});

var EmotionProcess = sequelize.define('EmotionProcess', {
  ProcessID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  Status: {type:Sequelize.STRING, defaultValue: 'unprocess'},
  Killed: {type:Sequelize.INTEGER, defaultValue: 0},
  Orientation:{type:Sequelize.INTEGER, defaultValue: 0},
  
},{timestamps: true,freezeTableName: true,});

var AudioProcess = sequelize.define('AudioProcess', {
  ProcessID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  Status: {type:Sequelize.STRING, defaultValue: 'unprocess'},
  Killed: {type:Sequelize.INTEGER, defaultValue: 0},
  
},{timestamps: true,freezeTableName: true,});

var UploadProcess = sequelize.define('UploadProcess', {
  ProcessID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  Status: {type:Sequelize.STRING, defaultValue: 'unprocess'},
  Killed: {type:Sequelize.INTEGER, defaultValue: 0},
  
},{timestamps: true,freezeTableName: true,});

var EmotionResult = sequelize.define('EmotionResult', {
  ResultID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  EmotionInfo: {type:Sequelize.STRING, defaultValue: ''},
  Vlog: {type:Sequelize.INTEGER,},  
},{timestamps: true,freezeTableName: true,});

var AudioResult = sequelize.define('AudioResult', {
  ResultID: {type:Sequelize.INTEGER,primaryKey: true, autoIncrement: true,},
  SessionID: {type:Sequelize.STRING,},
  AnswerWords: {type:Sequelize.STRING, defaultValue: ''},
  
},{timestamps: true,freezeTableName: true,});

module.exports = {Job,Item,Session,GenerateProcess,Interview,User,UserProfile,Question,Question_view,Vendor,ProcessCheck,NanaProcess,EmotionProcess,AudioProcess,UploadProcess,AudioResult,EmotionResult,Purveyor}


