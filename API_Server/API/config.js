//-----------外部APIconfig-------------//

 const UploadfilePath = './files/';
 const Uploadfile_File_Extension = '.mkv';
 const SpeechToText_File_Extension = '.aac';
 const Google_File_Extension = '.mp3';
 const SSL_PrivateKey = 'private.key';
 const SSL_Certificate = 'certificate.crt';
 const SSL_Ca = 'ca_bundle.crt';
 const Google_Cloud_SpeechToText_URL = 'https://speech.googleapis.com/v1/speech:longrunningrecognize?key=';
 const Google_Cloud_SpeechToText_API_Key = 'AIzaSyApfJJCHoppZn5JX2dDrJu4cdUlJVCw-eo';
 const Create_FollowQuestion_URL = 'https://interview.54ucl.com:7777/v2/Com/CreateFollowQuestion';
 const Conver_AAC_To_Mp3 = './conver_aac_mp3.py';
 const Google_Cloud_GetRecognizeResult = './GetGoogleResult.py';

//-----------DB_module_config-------------//

 const DB_Master_Name = 'i2aFor1111_Test';
 const DB_Question_Name = 'i2aFor1111_Question';
 const DB_Master_Account = 'root';
 const DB_Question_Account = 'root';
 const DB_Master_Password = 'nkustucl';
 const DB_Question_Password = 'nkustucl';
 const DB_Master_IP = 'localhost';
 const DB_Question_IP = 'localhost';

module.exports = {
    UploadfilePath,
    Uploadfile_File_Extension,
    SpeechToText_File_Extension,
    Google_File_Extension,
    SSL_PrivateKey,
    SSL_Certificate,
    SSL_Ca,
    Google_Cloud_SpeechToText_URL,
    Google_Cloud_SpeechToText_API_Key,
    Create_FollowQuestion_URL,
    Conver_AAC_To_Mp3,
    Google_Cloud_GetRecognizeResult,
    DB_Master_Name,
    DB_Question_Name,
    DB_Master_Account,
    DB_Question_Account,
    DB_Master_Password,
    DB_Question_Password,
    DB_Master_IP,
    DB_Question_IP
}


