﻿
var express = require('express');
// 建立 express 例項
var db = require('./db_module');
const app = express();
var fs = require('fs');
const path = require('path');
var uuidv1 = require('uuid/v1');
const bodyParser = require('body-parser');
const multer = require('multer');
var dgram = require('dgram');

app.use(bodyParser.json({ limit: '1mb' }));  //body-parser 解析json格式数据
app.use(bodyParser.urlencoded({            //此项必须在 bodyParser.json 下面,为参数编码
  extended: true
}));

app.post('/v2/Session/SearchGenerateProcess', (req, res) => {

  db.ProcessCheck.count().then(function (count) {
    if (count > 5) {
      return res.send("None")
    } else {
      db.GenerateProcess.findAll({
        where: {
          Status: 'unprocess'
        }
      }).then(function (GP_result) {
        console.log(GP_result.length)
        if (GP_result.length >= 1) {
          db.Interview.findAll({
            where: {
              InterviewID: GP_result[0].InterviewID,
              OfficeID: { $gt: 1 }
            }
          }).then(function (interview) {
            if (interview.length > 0) {
              data = {
                InterviewID: interview[0].InterviewID,
                SessionID: "",
                OfficeID: interview[0].OfficeID
              }

              db.UserProfile.findAll({
                where: {
                  ProfileID: interview[0].ProfileID
                }
              }).then(function (userprofile) {
                console.log('userprofile:' + userprofile.length)
                profile = JSON.parse(userprofile[0].Others)
                console.log(profile.InterviewPosition)
                response = {
                  InterviewID: data.InterviewID,
                  SessionID: "",
                  OfficeID: data.OfficeID,
                  Profile: profile
                }
                console.log(response)
                return res.send(response)
              })
            }
          }).catch(function (err) {
            
            console.log(err.message)
          })
        } else {
          return res.send("None")
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Session/SearchNaNaProcess', (req, res) => {
  db.ProcessCheck.count().then(function (count) {
    if (count > 5) {
      return res.send("None")
    } else {
      db.NanaProcess.findAll({
        where: {
          Status: 'unprocess'
        },
        attributes: ['SessionID']
      }).then(function (NP_result) {
        console.log(NP_result.length)
        if (NP_result.length >= 1) {
          data = {
            SessionID: NP_result[0].SessionID,
          }
          return res.send(data)
        } else {
          return res.send("None")
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Session/SearchAudioProcess', (req, res) => {

  db.ProcessCheck.count().then(function (count) {
    if (count > 5) {
      return res.send("None")
    } else {
      db.AudioProcess.findAll({
        where: {
          Status: 'unprocess'
        },
        attributes: ['SessionID']
      }).then(function (AP_result) {
        console.log(AP_result.length)
        //console.log(result[0].InterviewID)
        if (AP_result.length >= 1) {
          data = {
            SessionID: AP_result[0].SessionID,
          }
          return res.send(data)
        } else {
          return res.send("None")
        }
      }).catch(function (err) {
       
        console.log(err.message)
      })
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Session/SearchEmotionProcess', (req, res) => {

  db.ProcessCheck.count().then(function (count) {
    if (count > 5) {
      return res.send("None")
    } else {
      db.EmotionProcess.findAll({
        where: {
          Status: 'unprocess'
        },
        attributes: ['SessionID', 'Orientation']
      }).then(function (EP_result) {
        console.log(EP_result.length)
        if (EP_result.length >= 1) {
          db.Session.findAll({
            where: {
              SessionID: EP_result[0].SessionID
            },
          }).then(function (session) {
            if (session.length >= 1) {
              data = {
                SessionID: EP_result[0].SessionID,
                Orientation: EP_result[0].Orientation,
                ItemID: session[0].ItemID,
              }
              return res.send(data)
            } else {
              return res.send("None")
            }
          }).catch(function (err) {
            
            console.log(err.message)
          })
        }else {
          return res.send("None")
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Session/SearchUploadProcess', (req, res) => {

  db.ProcessCheck.count().then(function (count) {
    if (count > 5) {
      return res.send("None")
    } else {
      db.UploadProcess.findAll({
        where: {
          Status: 'unprocess'
        },
        attributes: ['SessionID']
      }).then(function (UP_result) {
        console.log(UP_result.length)
        //console.log(result[0].InterviewID)
        if (UP_result.length >= 1) {
          data = {
            SessionID: UP_result[0].SessionID,
          }
          return res.send(data)
        } else {
          return res.send("None")
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Process/InitProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var time = parseInt(req.body.Time)
  var pid = req.body.PID
  var host = req.body.Host
  var interview = req.body.InterviewID
  var agent = req.body.Agent
  console.log(time)
  db.ProcessCheck.create({
    Time: time,
    PID: pid,
    Host: host,
    InterviewID: interview,
    SessionID: sessionid,
    Agent: agent,
  }).then(function (result) {
    return res.send({
      "Status": 'ok'
    })
  }).catch(function (err) {
    
    console.log(err.message);
  });
});

app.post('/v2/Process/CheckSubProcess', (req, res) => {

  var host = req.body.Host;
  db.ProcessCheck.findAll({
    where: { Host: host },
    Order: ["Time"]
  }).then(function (result) {
    if (result.length >= 1) {
      var data = {
        PID: result[0].PID,
        Time: result[0].Time,
        Host: host,
      }
      return res.send(data)
    } else {
      return res.send("None");
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Process/KillProcess', (req, res) => {

  var host = req.body.Host;
  var pid = req.body.PID;
  var killagent;

  db.ProcessCheck.findOne({
    where: { Host: host, PID: pid }
  }).then(function (result) {
    killagent = result.Agent
    console.log(result.Agent)
    var sessionid = result.SessionID
    if (killagent == 'GenerateProcess') {
      db.GenerateProcess.findOne({
        where: { InterviewID: result.InterviewID }
      }).then(function (result1) {
        if (result1.Killed < 2) {
          result1.update({
            Killed: (result1.Killed + 1),
            Status: 'unprocess'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'ok' })
        } else {
          result1.update({
            Status: 'crash'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'Crash' })
        }
        //console.log(result1.Killed)
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (killagent == 'UploadProcess') {
      db.UploadProcess.findOne({
        where: { SessionID: sessionid }
      }).then(function (result1) {
        if (result1.Killed < 2) {
          result1.update({
            Killed: (result1.Killed + 1),
            Status: 'unprocess'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'ok' })
        } else {
          result1.update({
            Status: 'crash'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'Crash' })
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (killagent == 'EmotionProcess') {
      db.EmotionProcess.findOne({
        where: { SessionID: sessionid }
      }).then(function (result1) {
        if (result1.Killed < 2) {
          result1.update({
            Killed: (result1.Killed + 1),
            Status: 'unprocess'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            console.log(err.message)
          })

          return res.send({ Status: 'ok' })
        } else {
          result1.update({
            Status: 'crash'
          })

          db.EmotionResult.update({
            EmotionInfo: 'crash'
          },
            {
              where: { SessionID: sessionid }
            })

          db.Session.findOne({
            where: { SessionID: sessionid }
          }).then(function (session) {
            db.Interview.findOne({
              where: { InterviewID: session.InterviewID }
            }).then(function (interview) {
              var E_status = interview.EmotionStatus + 1
              interview.update({ EmotionStatus: E_status })
            })
          }).catch(function (err) {
            console.log(err.message)
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'Crash' })
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (killagent == 'AudioProcess') {
      db.AudioProcess.findOne({
        where: { SessionID: sessionid }
      }).then(function (result1) {
        if (result1.Killed < 2) {
          result1.update({
            Killed: (result1.Killed + 1),
            Status: 'unprocess'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            console.log(err.message)
          })

          return res.send({ Status: 'ok' })
        } else {
          result1.update({
            Status: 'crash'
          })

          db.AudioResult.update({
            AnswerWords: 'crash'
          },
            {
              where: { SessionID: sessionid }
            })

          db.Session.findOne({
            where: { SessionID: sessionid }
          }).then(function (session) {
            db.Interview.findOne({
              where: { InterviewID: session.InterviewID }
            }).then(function (interview) {
              var A_status = interview.AudioStatus + 1
              interview.update({ AudioStatus: A_status })
            })
          }).catch(function (err) {
            console.log(err.message)
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'Crash' })
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (killagent == 'NaNaProcess') {
      db.NanaProcess.findOne({
        where: { SessionID: sessionid }
      }).then(function (result1) {
        if (result1.Killed < 2) {
          result1.update({
            Killed: (result1.Killed + 1),
            Status: 'unprocess'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'ok' })
        } else {
          result1.update({
            Status: 'crash'
          })

          db.ProcessCheck.destroy({
            where: { Host: host, PID: pid }
          }).catch(function (err) {
            
            console.log(err.message)
          })

          return res.send({ Status: 'Crash' })
        }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
    //console.log(result.Agent)

  }).catch(function (err) {
    
    console.log(err.message)
  })
});

app.post('/v2/Process/UpdateProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var time = req.body.Time
  var status = req.body.Status
  var interviewid = req.body.InterviewID
  var agent = req.body.Agent
  if (interviewid == 0) {
    if (status == 'Processed') {
      db.ProcessCheck.destroy({
        where: { SessionID: sessionid }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (status == 'Processing') {
      db.ProcessCheck.update({ Time: time }, {
        where: { SessionID: sessionid, Agent: agent }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  } else {
    if (status == 'Processed') {
      db.ProcessCheck.destroy({
        where: { InterviewID: interviewid }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    } else if (status == 'Processing') {
      db.ProcessCheck.update({ Time: time }, {
        where: { InterviewID: interviewid }
      }).catch(function (err) {
        
        console.log(err.message)
      })
    }
  }

  return res.send({ Status: 'ok' })
});

app.post('/v2/Process/UpdateUploadProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var status = req.body.Status
  var agent = req.body.Agent
  db.UploadProcess.update({ Status: status }, {
    where: { SessionID: sessionid }
  }).catch(function (err) {
    
    console.log(err.message)
  })

  return res.send({ Status: 'ok' })
});


app.post('/v2/Process/UpdateNaNaProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var status = req.body.Status
  var agent = req.body.Agent
  db.NanaProcess.update({ Status: status }, {
    where: { SessionID: sessionid }
  }).then(function (result) {
    if (status == 'Processed') {
      db.Session.findAll({
        where: {
          SessionID: sessionid
        }
      }).then(function (session) {
        console.log(session[0].Orientation);
        console.log(typeof (session[0].Orientation))
        db.EmotionProcess.create({
          SessionID: sessionid,
          Orientation: session[0].Orientation
        }).catch(function (err) {
          
          console.lod(err.message)
        });
      }).catch(function (err) {
        console.log(err.message)
      });
      db.AudioProcess.create({
        SessionID: sessionid,
      }).catch(function (err) {
        
        console.lod(err.message)
      });
      db.UploadProcess.create({
        SessionID: sessionid,
      }).catch(function (err) {
        
        console.lod(err.message)
      });
      db.EmotionResult.create({
        SessionID: sessionid,
      }).catch(function (err) {
        
        console.lod(err.message)
      });
      db.AudioResult.create({
        SessionID: sessionid,
      }).catch(function (err) {
        
        console.lod(err.message)
      });
    }
  }).catch(function (err) {
    
    console.log(err.message)
  })

  return res.send({ Status: 'ok' })
});

app.post('/v2/Process/UpdateEmotionProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var status = req.body.Status
  var agent = req.body.Agent
  db.EmotionProcess.update({ Status: status }, {
    where: { SessionID: sessionid }
  }).then(function (session) {

    return res.send({ Status: 'ok' })
  }).catch(function (err) {
    
    console.log(err.message)
    return res.send({ Status: err.message })
  })

  
});


app.post('/v2/Process/UpdateAudioProcess', (req, res) => {

  var sessionid = req.body.SessionID
  var status = req.body.Status
  var agent = req.body.Agent
  db.AudioProcess.update({ Status: status }, {
    where: { SessionID: sessionid }
  }).catch(function (err) {
    
    console.log(err.message)
  })

  return res.send({ Status: 'ok' })
});


app.post('/v2/Process/UpdateGenerateProcess', (req, res) => {

  var interviewid = req.body.Interview
  var status = req.body.Status
  var agent = req.body.Agent
  db.GenerateProcess.update({ Status: status }, {
    where: { InterviewID: interviewid }
  }).catch(function (err) {
    
    console.log(err.message)
  })

  return res.send({ Status: 'ok' })
});


app.post('/v2/Process/UpdateEmotionAIResult', (req, res) => {

  var sessionid = req.body.SessionID
  var emotioninfo = req.body.EmotionInfo
  var vlog = req.body.VlogResult
  console.log(emotioninfo)
  db.EmotionResult.findAll({
    where: {
      SessionID: sessionid
    }
  }).then(function (Emotion_result) {
    if(Emotion_result[0].EmotionInfo==''){
      db.EmotionResult.update({ EmotionInfo: emotioninfo, Vlog: vlog }, {
        where: { SessionID: sessionid }
      }).then(function (result) {
    
        db.Session.findOne({
          where: { SessionID: sessionid }
        }).then(function (session) {
          db.Interview.findOne({
            where: { InterviewID: session.InterviewID }
          }).then(function (interview) {
            //console.log('InterviewID'+ result.InterviewID)
            //console.log('E_status:'+result.EmotionStatus)
            var E_status = interview.EmotionStatus + 1
            interview.update({ EmotionStatus: E_status })
          }).catch(function (err) {
            
            console.log(err.message)
            return res.send({ Status: err.message })
          })
        }).catch(function (err) {
          
          console.log(err.message)
          return res.send({ Status: err.message })
        })
        return res.send({ Status: 'ok' })
      }).catch(function (err) {
        
        console.log(err.message)
        return res.send({ Status: err.message })
      })
    }else{
      db.EmotionResult.update({ EmotionInfo: emotioninfo, Vlog: vlog }, {
        where: { SessionID: sessionid }
      }).then(function (interview) {
        return res.send({ Status: 'ok' })
      }).catch(function (err) {
        
        console.log(err.message)
        return res.send({ Status: err.message })
      })
    }
    console.log("EmotionResult:" + e_result.length)
  })
  
  

});

app.post('/v2/Process/UpdateAudioAIResult', (req, res) => {

  var sessionid = req.body.SessionID
  var answords = req.body.Words
  db.AudioResult.update({ AnswerWords: answords }, {
    where: { SessionID: sessionid }
  }).catch(function (err) {
    
    console.log(err.message)
  })

  db.Session.findOne({
    where: { SessionID: sessionid }
  }).then(function (session) {
    db.Interview.findOne({
      where: { InterviewID: session.InterviewID }
    }).then(function (interview) {
      var A_status = interview.AudioStatus + 1
      interview.update({ AudioStatus: A_status })
    })
  }).catch(function (err) {
    console.log(err.message)
  })

  return res.send({ Status: 'ok' })
});


app.listen(4003, () =>
  console.log(`Example app listening on port 4003!`),
);
