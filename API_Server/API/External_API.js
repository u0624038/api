﻿const config = require('./config.js');
var express = require('express');
// 建立 express 例項
var db = require('./db_module');
var request = require('request')
const app = express();
var fs = require('fs');
const path = require('path');
var uuidv1 = require('uuid/v1');
const bodyParser = require('body-parser');
const multer = require('multer');
var dgram = require('dgram');
var jsonfind = require('json-find');
var client = dgram.createSocket('udp4');
var UploadfilePath = config.UploadfilePath
var video_File_Extension = config.Uploadfile_File_Extension
var audio_File_Extension = config.SpeechToText_File_Extension


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, UploadfilePath);  // 儲存的路徑，備註：需要自己建立
  },
  filename: function (req, file, cb) {
    // 將儲存檔名設定
    cb(null, uuidv1() + video_File_Extension);
  }
});
var storageaac = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, UploadfilePath);  // 儲存的路徑，備註：需要自己建立
  },
  filename: function (req, file, cb) {
    // 將儲存檔名設定
    cb(null, uuidv1() + audio_File_Extension);
  }
});
var uploadaac = multer({ storage: storageaac })
const https = require('https');
var upload = multer({ storage: storage })
var privateKey = fs.readFileSync(config.SSL_PrivateKey, 'utf8');
var certificate = fs.readFileSync(config.SSL_Certificate, 'utf8');
var ca = fs.readFileSync(config.SSL_Ca, 'utf8');
var credentials = { key: privateKey, cert: certificate, ca: ca };
//var message = '';
app.use(bodyParser.json({ limit: '1mb' }));  //body-parser 解析json格式数据
app.use(bodyParser.urlencoded({            //此项必须在 bodyParser.json 下面,为参数编码
  extended: true
}));

let { PythonShell } = require('python-shell')
var error_str = ''

process.on('exit', function (err) {
  let option = {
    args:
      [
        error_str.stack
      ]
  }
  PythonShell.run('test_server_stop_email.py', option, function (err) {
    if (err) throw err;
    console.log('finished');
  });
  console.log('server close');
});


app.get('/', (req, res) => {
  console.log(req.headers);
  /*process.on('uncaughtException', function (err) {
    console.log(err);
    try {
        var killTimer = setTimeout(function () {
            process.exit(1);
        }, 10000);
        killTimer.unref();
        //server.close();
    } catch (e) {
        console.log('error when exit', e.stack);
    }
  });
  setTimeout(function () {
      // throws Error
      throw new Error('async exception'); 
  }, 1000);*/
  return res.send('ok');
})

function isDigit(s) {
  var patrn = /^[0-9]{1,20}$/;
  if (!patrn.exec(s)) {
    return false
  }
  return true
}

function isUUID(s) {
  var patrn = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/;
  if (!patrn.exec(s)) {
    return false
  }
  return true
}

app.post('/v2/Create/CreateUser', (req, res) => {
  var date = new Date();
  var userid = req.body.UserID;
  var profileid = req.body.ProfileID + date.getFullYear() + (date.getMonth() + 1) + date.getHours() + date.getMinutes() + date.getSeconds();
  var name = req.body.Profile.Name;
  var gender = req.body.Profile.Gender;
  console.log(jsonfind(req.body.Profile).checkKey('InterviewPosition'));
  var officeid = req.body.Profile.InterviewPosition;
  console.log(officeid)
  if (jsonfind(req.body.Profile).checkKey('InterviewPosition') == false || officeid == 0) {
    return res.send({ InterviewID: '未收到職務資訊，無法產生面試代碼，請重新使用' })
  }
  else {

    db.User.findAll({
      where: {
        UserID: userid
      }
    }).then(function (user) {
      if (user.length == 0) {
        db.User.create({
          UserID: userid,
        }).catch(function (err) {
          
          console.log(err.message);
        });
      }
    })

    db.UserProfile.create({
      ProfileID: profileid,
      Name: name,
      Gender: gender,
      Others: JSON.stringify(req.body.Profile),
      UserID: userid,
    }).catch(function (err) {

      console.log(err.message);
    });

    db.Interview.create({
      GenerateStatus: 0,
      EmotionStatus: 0,
      AudioStatus: 0,
      IssueStatus: 0,
      ProfileID: profileid,
      VendorID: 1,
      OfficeID: officeid,
    }).then(function (result) {
      console.log(result.InterviewID)
      db.GenerateProcess.create({
        Status: 'unprocess',
        killed: 0,
        InterviewID: result.InterviewID,
      }).catch(function (err) {
        console.log(err.message);
      });
      return res.send({ InterviewID: result.InterviewID })
    }).catch(function (err) {

      console.log(err.message);
    });
  }

});

app.post('/v2/Create/CreateVendor', (req, res) => {
  var vendorname = req.body.VendorName
  db.Vendor.findAll({
    where: {
      VendorID: 1
    }
  }).then(function (vendor) {
    if (vendor.length == 0) {
      db.Vendor.create({
        VendorID: 1,
        VendorName: vendorname
      })
    } else {
      db.Vendor.update({
        VendorName: vendorname
      }, {
          where: {
            VendorID: 1
          }
        })
    }
    return res.send({ Ststus: 'ok' })
  })

});

app.post('/v2/Session/ConnectInterview', (req, res) => {
  var id = req.body.InterviewID;

  console.log(id)
  console.log(jsonfind(req.body).checkKey('InterviewID'))
  if (jsonfind(req.body).checkKey('InterviewID') == false) {
    return res.send({ InterviewID: 'no key' })
  } else {
    if (isDigit(id.toString())) {
      db.Interview.findAll({
        where: {
          InterviewID: id
        }
      }).then(function (interview) {
        if (interview.length == 0) {
          return res.send({ InterviewID: 0 })
        } else if (interview[0].GenerateStatus == 0) {
          return res.send({ InterviewID: 1 })
        } else {
          db.Session.findAll({
            where: {
              InterviewID: id
            }
          }).then(function (session) {
            if (session.length == 0) {
              db.Item.findAll({
                where: {
                  InterviewID: id
                }, Order: ["ItemID"]
              }).then(function (result) {
                if (result.length == 0) {
                  return res.send({ InterviewID: 1 })
                }
                else {
                  console.log(result[0])
                  var count = 0;
                  var totaltime = 0;
                  var questionlist = [];
                  for (let i = 0; i < result.length; i++) {
                    db.Question.findAll({
                      where: {
                        ItemID: result[i].ItemID
                      }
                    }).then(function (result1) {

                      totaltime += result1[0].Time;
                      db.Question_view.findAll({
                        where: {
                          QID: result1[0].QuestionID
                        }
                      }).then(function (result2) {
                        var qjson = {
                          Content: result2[0].Content,
                          VideoPath: '',
                          ItemID: result1[0].ItemID,
                          Time: result1[0].Time,
                          VoicePath: result2[0].VoicePath
                        }
                        questionlist.push(qjson)

                        if (count == result.length - 1) {
                          questionlist.sort(function (a, b) {
                            return parseInt(a.ItemID) - parseInt(b.ItemID);
                          });
                          //console.log(questionlist)
                          var data = {
                            TotalTime: totaltime,
                            QuestionList: questionlist
                          }
                          return res.send(data)
                        }
                        count++
                      }).catch(function (err) {
                        console.log(err.message);
                      });

                    }).catch(function (err) {                  
                      console.log(err.message);
                    });

                  }
                }
              }).catch(function (err) {
                
                console.log(err.message);
              });
            } else {
              return res.send({ InterviewID: 2 })
            }
          }).catch(function (err) {
            
            console.log(err.message);
          })
        }
      }).catch(function (err) {
       
        console.log(err.message);
      })
    } else {
      return res.send({ InterviewID: -1 })
    }
  }
});

app.post('/v2/Session/RequestJobID', (req, res) => {

  db.Job.create({
    //JobID:c.JobID+1,
    MetaData: '',
    Path: '',
  }).then(function (result) {
    console.log("Create Jobid");
    //console.log(result.JobID);
    var str = { JobID: result.JobID }
    return res.send(str);
  }).catch(function (err) {
    console.log(err.message);
  });

});



app.post('/v2/Session/UploadFiles', upload.single('File'), (req, res) => {

  process.on('uncaughtException', function (err) {
    console.log(err);
    try {
      var killTimer = setTimeout(function () {
        //process.exit(1);
      }, 30000);
      killTimer.unref();
      //server.close();
    } catch (e) {
      console.log('error when exit', e.stack);
    }
  });

  var file = req.file;

  console.log('原始名稱：%s', req.file.originalname);
  console.log('檔案名稱：%s', file.filename);
  console.log('檔案大小：%s', file.size);
  console.log('檔案路徑：%s', file.path);


  var jobid = req.headers.jobid;

  var sessionid = req.headers.sessionid;
  var datasize = req.headers.datasize;
  var metadata = {
    JobID: jobid,
    SessionID: sessionid,
    DataSize: datasize,
    Path: './files/' + (sessionid + '.mkv')
  }

  metadata = JSON.stringify(metadata)
  //console.log(typeof(metadata_json))
  //console.log(file);
  //console.log(sessionid);
  console.log(jobid);
  var original = req.file.originalname.split('.')

  if (original.length == 1) {
    return res.send({ Status: 'file has not file extension' })
  } else {
    if (req.file.originalname.split('.')[1] != 'mkv') {
      return res.send({ Status: 'file format is not mkv' })
    } else {
      if (file.size == 1) {
        return res.send({ Status: 'upload empty file' })
      } else {
        if (jsonfind(req.headers).checkKey('sessionid') == false || jsonfind(req.headers).checkKey('jobid') == false || jsonfind(req.headers).checkKey('datasize') == false) {
          return res.send({ Status: 'no key or JobID and DataSize must > 0' })
        } else {
          if (isDigit(datasize.toString()) && isDigit(jobid.toString())) {
            if (isUUID(sessionid.toString())) {

              fs.rename(('./files/' + file.filename), ('./files/' + (sessionid + '.mkv')), function (err) {
                if (err) throw err;

                db.Job.findAll({
                  where: {
                    JobID: jobid
                  }
                }).then(function (result) {
                  if (result.length >= 1) {
                    result[0].update({
                      MetaData: metadata,
                      Path: ('./files/' + (sessionid + '.mkv'))
                    }).then(function (result1) {
                      console.log("Upload Sucess");
                      var str = { Status: "ok" }
                      return res.send(str);
                    }).catch(function (err) {
                      
                      console.log(err.message);
                      return res.send({ Status: err.message })
                    });
                  } else {
                    return res.send({ Status: 'can not find this JobID' })
                  }
                }).catch(function (err) {
                  
                  console.log(err.message);
                  return res.send({ Status: err.message })
                });
                //console.log(createjob.create_jobid(jobid,metadata,('./files/'+sessionid+'.mkv')))

              });
            } else {
              return res.send({ Status: 'error SessionID is not correct' })
            }
          } else {
            return res.send({ Status: 'error ItemID or Job is not a int' })
          }
        }
      }
    }
  }
});



app.post('/v2/Session/CheckFiles', (req, res) => {
  var id = req.body.JobID;
  if (jsonfind(req.body).checkKey('JobID') == false) {
    return res.send({ Result: 'no key' })
  } else {
    if (isDigit(id.toString())) {
      db.Job.findAll({
        where: {
          JobID: id
        }
      }).then(function (result) {
        console.log('query all users');
        var path = result[0].Path;

        var MetaData = JSON.parse(result[0].MetaData);
        var sessionid = MetaData.SessionID;
        var db_size = MetaData.DataSize;
        console.log(db_size)
        fs.stat(path, (err, stats) => {
          if (err) throw err;
          console.log(stats.size)
          if (stats.size == db_size) {
            db.Session.update({
              AVPath: path,
            }, { where: { SessionID: sessionid } }).then(function (result1) {
              console.log("update session sucess")
            }).catch(function (err) {            
              console.log(err.message);
            });
            db.NanaProcess.create({
              SessionID: sessionid,
            }).catch(function (err) {
          
              console.log(err.message)
              return res.send({ Result: err.message });
            });

            return res.send({ Result: true });
          }
          else {
            return res.send({ Result: false });
          }
        });
      });
    } else {
      return res.send({ Result: 'Job is not a int' });
    }
  }
});

app.post('/v2/Session/GetSessionID', (req, res) => {
  var id = req.body.InterviewID;
  var sessionid = uuidv1();
  if (jsonfind(req.body).checkKey('InterviewID') == false) {
    return res.send({ SessionID: 'no key' })
  } else {
    if (isDigit(id.toString())) {
      db.Session.create({
        SessionID: sessionid,
        InterviewID: id,
      }).then(function (result) {
        console.log("Create Session");
        var str = { SessionID: sessionid }
        return res.send(str);
      }).catch(function (err) {    

        console.log(err.message);
        return res.send({ SessionID: err.message });
      });
    } else {
      return res.send({ SessionID: 'error ID is not a int' })
    }
  }
});

app.post('/v2/Session/CreateAnswer', (req, res) => {
  var sessionid = req.body.SessionID;
  var itemid = req.body.ItemID;
  if (jsonfind(req.body).checkKey('SessionID') == false || jsonfind(req.body).checkKey('ItemID') == false) {
    return res.send({ Status: 'no key' })
  } else {
    if (isDigit(itemid.toString())) {
      if (isUUID(sessionid.toString())) {
        db.Session.update({
          ItemID: itemid
        }, {
            where: { SessionID: sessionid }
          }).then(function (result1) {
            console.log("update sucess")
            return res.send({ Status: "ok" })
          }).catch(function (err) {
            
            console.log(err.message);
            return res.send({ SessionID: err.message });
          });
      } else {
        return res.send({ Status: 'error SessionID is not correct' })
      }
    } else {
      return res.send({ Status: 'error ItemID is not a int' })
    }
  }
});

app.post('/v2/Session/GetVideoPath', (req, res) => {
  var id = req.body.InterviewID;

  db.Session.findAll({
    where: {
      InterviewID: id
    }
  }).then(function (result) {
    var videolist = [];
    var count = 0;
    for (let i = 0; i < result.length; i++) {
      if (result[i].AVPath != '' && result[i].ItemID != '') {
        var pathjson = {
          ItemID: result[i].ItemID,
          Path: result[i].AVPath

        }
        videolist.push(pathjson)
      }
      if (count == result.length - 1) {
        var data = {
          VideoPathList: videolist
        }
        return res.send(data)
      }
      count++
    }
  }).catch(function (err) {
    
    console.log(err.message);
  });

});

app.post('/v2/Session/SpeechToText', uploadaac.single('File'), (req, res) => {
  //app.post('/v2/Session/SpeechToText', (req, res) => {

  var file = req.file;
  console.log('檔案名稱: ', file.filename)

  let options = {
    args:
      [
        file.filename,

      ]
  }

  PythonShell.run(config.Conver_AAC_To_Mp3, options, (err, data) => {
    if (err) throw (err)
    //const parsedString = JSON.parse(data)
    //console.log(file.filename.split('.')[0])
    const fileName = config.UploadfilePath + file.filename.split('.')[0] + config.Google_File_Extension;
    //const fileName = './files/test.mp3';
    console.log(fileName)
    file = fs.readFileSync(fileName);
    const audioBytes = file.toString('base64');
    
    var data = {
      "audio": {
        "content": audioBytes,
      },
      "config": {
        //"enableAutomaticPunctuation": t,
        "encoding": "ENCODING_UNSPECIFIED",
        "sampleRateHertz": 16000,
        "languageCode": "cmn-Hant-TW",
        //"model": "default"
      }
    }
    var json = JSON.stringify(data);
    var API_KEY = config.Google_Cloud_SpeechToText_API_Key
    var url = config.Google_Cloud_SpeechToText_URL + API_KEY
    console.log(url)
    request.post({
      headers: { 'content-type': 'application/json' },
      url: url,
      body: json
    }, function (error, response, body) {
      console.log(JSON.parse(body))
      //console.log(JSON.parse(body)["results"][0])
      var name = JSON.parse(body)["name"]
      console.log(name)

      let options = {
        args:
          [
            name,
          ]
      }

      PythonShell.run(config.Google_Cloud_GetRecognizeResult, options, (err, data) => {
        if (err) throw (err)
        //const parsedString = JSON.parse(data)
        console.log(data)
        return res.send({ Result: data[0] })
      })

    })
  })

});

app.post('/v2/Session/FollowQuestion', (req, res) => {

  //console.log(req.body)
  var words = req.body.Words;
  var itemid = req.body.ItemID;
  var QID = ''
  console.log(words)
  console.log(itemid)
  db.Question.findAll({
    where: {
      ItemID: itemid
    }
  }).then(function (question) {

    QID = question[0].QuestionID
    console.log(QID)
    var data = { 'ItemID': itemid, 'QID': QID, 'Follow_word': words }
    var json = JSON.stringify(data);
    console.log(json)
    request.post({
      headers: { 'content-type': 'application/json' },
      url: config.Create_FollowQuestion_URL,
      body: json
    }, function (error, response, body) {
      console.log(body);
      console.log(JSON.parse(body)['QID']);
      var itemid = JSON.parse(body)['ItemID']
      var qid = [];
      if (JSON.parse(body)['QID'] != 'None') {
        qid.push(JSON.parse(body)['QID']);
        db.Question_view.findAll({
          where: {
            QID: qid[0]
          }
        }).then(function (question2) {
          console.log(question2[0].Content)
          return res.send({ Question: question2[0].Content, ItemID: itemid })
        })
      } else {
        return res.send(body)
      }

    });

  })

});


const httpsServer = https.createServer(credentials, app);

httpsServer.listen(3004, '0.0.0.0', () => {
  console.log('HTTPS Server running on port 3004');

});


/*app.listen(3004, () =>
  console.log(`Example app listening on port 3004!`),
);*/
