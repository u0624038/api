#coding:utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, UniqueConstraint, Index
from sqlalchemy import Integer, String, VARCHAR, TEXT, BOOLEAN
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from sqlalchemy import desc
import os, shutil
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
user = config["DB"]["user"]
password = config["DB"]["password"]
host = config["DB"]["host"]
MDB_name = config["DB"]["MDB_name"]

DBInfo = "mysql+pymysql://"+ user +":"+password+ "@" + host+ "/" + MDB_name+"?charset=utf8mb4"

DBInfo = "mysql+pymysql://root:c217db@163.18.26.141/i2aFor1111_Test?charset=utf8mb4"
#DBInfo = "mysql+pymysql://root:honyi1013@127.0.0.1/i2aFor1111?charset=utf8mb4"
DBLink = create_engine(DBInfo, poolclass=NullPool)
Base = declarative_base()


class Interview(Base):
    __tablename__ = "Interview"
    InterviewID = Column(Integer, primary_key=True)
    GenerateStatus = Column(BOOLEAN)
    EmotionStatus = Column(BOOLEAN)
    AudioStatus = Column(BOOLEAN)
    IssueStatus = Column(Integer)
    OfficeID = Column(Integer)
    ProfileID = Column(VARCHAR(255))
    VendorID = Column(Integer)


    def __init__(self, **kwargs):
        self.InterviewID = kwargs["InterviewID"] if "InterviewID" in kwargs else 0
        self.GenerateStatus = kwargs["GenerateStatus"] if "GenerateStatus" in kwargs else 0
        self.EmotionStatus = kwargs["EmotionStatus"] if "EmotionStatus" in kwargs else 0
        self.AudioStatus = kwargs["AudioStatus"] if "AudioStatus" in kwargs else 0
        self.IssueStatus = kwargs["IssueStatus"] if "IssueStatus" in kwargs else 0
        self.OfficeID = kwargs["OfficeID"] if "OfficeID" in kwargs else 0
        self.ProfileID = kwargs["ProfileID"] if "ProfileID" in kwargs else ""
        self.VendorID = kwargs["VendorID"] if "VendorID" in kwargs else 0


class Question(Base):
    __tablename__ = "Question"
    #ID = Column(Integer, primary_key=True)
    QuestionID = Column(Integer)
    Language = Column(VARCHAR(255))
    Time = Column(Integer)
    AudioURL = Column(VARCHAR(255))
    ItemID = Column(Integer, primary_key=True)


    def __init__(self, **kwargs):
        #self.ID = kwargs["ID"] if "ID" in kwargs else 0
        self.QuestionID = kwargs["QuestionID"] if "QuestionID" in kwargs else 0
        self.Language = kwargs["Language"] if "Language" in kwargs else ""
        self.Time = kwargs["Time"] if "Time" in kwargs else 0
        self.AudioURL = kwargs["AudioURL"] if "AudioURL" in kwargs else ""
        self.ItemID = kwargs["ItemID"] if "ItemID" in kwargs else 0


class Item(Base):
    __tablename__ = "Item"
    ItemID = Column(Integer, primary_key=True)
    InterviewID = Column(Integer)
    

    def __init__(self, **kwargs):
        self.ItemID = kwargs["ItemID"] if "ItemID" in kwargs else 0
        self.InterviewID = kwargs["InterviewID"] if "InterviewID" in kwargs else 0


class GenerateProcess(Base):
    __tablename__ = "GenerateProcess"
    ProcessID = Column(Integer, primary_key=True)
    Status = Column(VARCHAR(255))
    Killed = Column(Integer)
    InterviewID = Column(Integer)
    

    def __init__(self, **kwargs):
        argList = {
            "Status": self.Status, 
            "Killed": self.Killed, 
            "InterviewID": self.InterviewID, 
        }
        for key, value in kwargs.items():
            if key in list(argList.keys()):
                argList[key] = value




def init_db():
    Base.metadata.create_all(DBLink)


def drop_db():
    Base.metadata.drop_all(DBLink)

init_db()
