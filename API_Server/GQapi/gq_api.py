#coding:utf-8
from flask import Flask, request
import json
import api_orm as ApiORM
import logging
import socket
import time
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
host = config["SSL"]["host"]
port = config["SSL"]["port"]
app = Flask(__name__)

Session = ApiORM.sessionmaker(bind=ApiORM.DBLink)
#session = Session()
logging.basicConfig(filename='gq_api.log', level=logging.INFO)


def udpsplunk(msg):
    UDP_IP = "163.18.26.108"
    UDP_PORT = 7602
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(json.dumps(msg), "utf-8"), (UDP_IP, UDP_PORT))


    
@app.route("/v2/Process/GenerateQuestion", methods=["POST"])
def GenerateQuestion():
    session = Session()
    IIDlist =[]
    reqData = json.loads(bytes.decode(request.data))
    # print(reqData)
    #print(reqData["InterviewID"])
    ALL_InterviewID = reqData["InterviewID"]
    print(type(ALL_InterviewID))
    print(ALL_InterviewID)
    if type(ALL_InterviewID).__name__=='list':
        IIDlist.extend(ALL_InterviewID)
        Interviewer = "1111"
    else:
        IIDlist.append(ALL_InterviewID)  
        Interviewer = "self"
    #print(ALL_InterviewID)
    #print(type(ALL_InterviewID))
    #IIDlist.extend(ALL_InterviewID)
    QIDs = reqData["QuestionID"]
    print(QIDs)
    questionCount = len(QIDs)
    for IID in IIDlist:
        InterviewID = int(IID)
        itemIDs = createNewItem(InterviewID, questionCount)
        Success = linkItemAndQuestion(itemIDs, QIDs)
        try:
            interviewUpdate = session.query(ApiORM.Interview).\
                filter(ApiORM.Interview.InterviewID == InterviewID).\
                update({"GenerateStatus": 1})
            session.commit()
            #session.close()
            print(IID, " GQ OK")
        except Exception as e:
            print(e)
            sesson.rollback()
            print(IID, " GQ ERROR")
        finally:
            session.close()
            #session.close()
    msg = {"APIName":"GenerateQuestion", 
            "ServerIP":"163.18.2.36",
            "Request_IP": str(request.remote_addr), 
            "Request_Data": reqData,
            "Interviewer":Interviewer,
            "server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
    udpsplunk(msg)
    return json.dumps({"Status": "ok"})


'''agent status interview'''
@app.route("/v2/Process/UpdateGenerateProcess", methods=["POST"])
def UpdateGenerateProcess():
    session = Session()
    # print("44444444")
    IIDlist =[]
    reqData = json.loads(bytes.decode(request.data))
    # print(reqData)
    ALL_InterviewID = reqData["InterviewID"]
    #print(type(ALL_InterviewID))
    #print(ALL_InterviewID)
    if type(ALL_InterviewID).__name__=='list':
        IIDlist.extend(ALL_InterviewID)
        Interviewer = "1111"
    else:
        IIDlist.append(ALL_InterviewID)
        Interviewer = "self"
    #print(type(ALL_InterviewID))
    #print(ALL_InterviewID)
    #for iid in ALL_InterviewID:
    #    IIDlist.append(int(iid))
    #IIDlist.extend(ALL_InterviewID)
    for IID in IIDlist:
        InterviewID = int(IID)
        # print(InterviewID, " GenerateStatus OK")
        # interviewStatus = session.query(ApiORM.Interview).filter(ApiORM.Interview.InterviewID == InterviewID).first()
        # print(IID, interviewStatus.GenerateStatus, "UPGQ")
        # if interviewStatus.GenerateStatus:
        try:
            #session.rollback()
            #session.commit()
            # session.close()
            ProcessUpdate = session.query(ApiORM.GenerateProcess).filter(ApiORM.GenerateProcess.InterviewID == InterviewID).update({"Status": reqData["Status"]})
            session.commit()
            #session.close()
            print(IID, " GenerateStatus OK")
        except Exception as e:
            print(e)
            print(IID, " GenerateStatus ERROR")
            session.rollback()
        finally:
            session.close()
    msg = {"APIName":"UpdateGenerateProcess", 
            "ServerIP":"163.18.2.36",
            "Request_IP": str(request.remote_addr), 
            "Request_Data": reqData,
            "Interviewer":Interviewer,
            "server_time": time.strftime('%A %B, %d %Y %H:%M:%S')}
    udpsplunk(msg)
    return json.dumps({"Status": "ok"})


'''interviewid language=zh_tw questionid videopath= time=120'''
'''createNewItem->takeQuestion/createQuestion->API:UpdateGenerateProcess'''
def createNewItem(InterviewID, QuestionCount):
    #print(InterviewID+"22222222222")
    session = Session()
    newItemIDs = []
    ItemID_Base = InterviewID * 100
    ############  更新Item TABLE 把ItemID 對應到 interviewID##########
    try:
        sameInterviewLastID = session.query(ApiORM.Item).\
            filter(ApiORM.Item.InterviewID == InterviewID).\
            order_by(ApiORM.Item.ItemID.desc()).first()
        if sameInterviewLastID != None:
            ItemID_Base = sameInterviewLastID.ItemID
        for i in range(QuestionCount):
            #session.commit()
            newItemIDs.append(ItemID_Base + i + 1)
            newItem = ApiORM.Item(ItemID=newItemIDs[i], InterviewID=InterviewID)
            session.add(newItem)
        session.commit()
    except:
        print(InterviewID, "CreateNewItem ERROR")
        session.rollback()
    finally:
        session.close()
    #session.commit()
    #session.close()
    ############ 最後回覆ItemID list ###########
    return newItemIDs


def linkItemAndQuestion(ItemIDs, QIDs):
    session = Session()
    #print("3333333")
    #checkQuestionID = session.query(ApiORM.Question).\
    #    order_by(ApiORM.Question.ID.desc()).first()
    #newQuestionID = -1
    #if checkQuestionID == None:
    #    newQuestionID = 1
    #else:
    #    newQuestionID = int(checkQuestionID.ID) + 1
    #newQuestionIDs = []
    #for i in range(len(ItemIDs)):
    #    newQuestionIDs.append(i + newQuestionID)
    ############  更新Queestion TABLE 把itemID QuestionID 做對應 ##########
    try:
        for i in range(len(ItemIDs)):
        # print(ItemIDs[i])
        #session.commit()
            newQuestion = ApiORM.Question(
                QuestionID=QIDs[i], 
                Language="zh_tw", 
                Time=120, 
                ItemID=ItemIDs[i], 
                AudioURL=""
            )
            session.add(newQuestion)
        session.commit()
    except:
        print(ItemIDs, "ERROR ADD")
        session.rollback()
    finally:
        session.close()
    #session.commit()
    #session.close()
    return len(ItemIDs)

if __name__ == "__main__":
    app.run(host=host, port=port)
