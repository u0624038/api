#coding:utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, UniqueConstraint, Index # 元素/主key
from sqlalchemy import Integer, String, VARCHAR, TEXT
from sqlalchemy.orm import sessionmaker, relationship, backref # 創接口/建立關系relationship(table.ID)
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool# sqlalchemy 查詢前連結，结束後，調用 session.close() 關閉連結
from sqlalchemy import desc #降序
import os, shutil
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
user = config["DB"]["user"]
password = config["DB"]["password"]
host = config["DB"]["host"]
QDB_name = config["DB"]["QDB_name"]

DBInfo = "mysql+pymysql://"+ user +":"+password+ "@" + host+ "/" + QDB_name+"?charset=utf8mb4"
DBLink = create_engine(DBInfo, poolclass=NullPool)# 創建一個空的資料庫
Base = declarative_base()  # 創數據結構放資料


class PositionQuestion(Base):
    __tablename__ = "PositionQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    QuestionType = Column(TEXT)
    SuggestAnswer = Column(TEXT, nullable=False)
    Tag = Column(TEXT)
    SP = Column(TEXT)
    Class = Column(TEXT)
    PositionID = Column(VARCHAR(6))
    DeleteStatus = Column(VARCHAR(1), nullable=False)
    VoicePath = Column(TEXT, nullable=False)
    # FollowQuestionID = Column(TEXT) #冠穎新增的
    def __init__(self, QuestionID, QuestionNum, Question, QuestionType, PositionID, Class, Tag, SP, **kwargs):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.QuestionType = QuestionType
        self.PositionID = PositionID
        self.Class = Class
        self.Tag = Tag
        self.SP = SP
        self.SuggestAnswer = kwargs["SuggestAnswer"] if "SuggestAnswer" in kwargs else None
        self.DeleteStatus = kwargs["DeleteStatus"] if "DeleteStatus" in kwargs else None
        self.VoicePath = kwargs["VoicePath"] if "VoicePath" in kwargs else None
        # self.FollowQuestionID = kwargs["FollowQuestionID"] if "FollowQuestionID" in kwargs else None
# PositionID	QuestionType	SP	Tag	Class	Question	SuggestAnswer


class GeneralQuestion(Base):
    __tablename__ = "GeneralQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    SuggestAnswer = Column(TEXT, nullable=False)
    Tag = Column(TEXT)
    Class = Column(TEXT)  
    DeleteStatus = Column(VARCHAR(1), nullable=False)
    VoicePath = Column(TEXT, nullable=False)
    # FollowQuestionID = Column(TEXT)  # 冠穎新增的
    def __init__(self, QuestionID, QuestionNum, Question, Class, Tag, **kwargs):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.Class = Class
        self.Tag = Tag
        self.SuggestAnswer = kwargs["SuggestAnswer"] if "SuggestAnswer" in kwargs else None
        self.DeleteStatus = kwargs["DeleteStatus"] if "DeleteStatus" in kwargs else None
        self.VoicePath = kwargs["VoicePath"] if "VoicePath" in kwargs else None
        # self.FollowQuestionID = kwargs["FollowQuestionID"] if "FollowQuestionID" in kwargs else None



class Position(Base):
    __tablename__ = "Position"
    PositionID = Column(VARCHAR(6), primary_key=True)
    Position = Column(VARCHAR(50))

    def __init__(self, PositionID, Position):
        self.PositionID = PositionID
        self.Position = Position
        

class Tag(Base):
    __tablename__ = "Tag"
    TagID = Column(Integer, primary_key=True)
    Tag = Column(VARCHAR(4))
    SuggestAnswer = Column(TEXT)

    def __init__(self, TagID, Tag, SuggestAnswer):
        self.TagID = TagID
        self.Tag = Tag
        self.SuggestAnswer = SuggestAnswer

#冠穎新增的
class FollowQuestion(Base):
    __tablename__ = "FollowQuestion"
    QuestionID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Question = Column(TEXT)
    VoicePath = Column(TEXT, nullable=True)
    VoiceStatus = Column(TEXT, nullable=True)
    # Tag = Column(TEXT)
    MainQuestionNum = Column(TEXT) #主題目的QuestionNum
    def __init__(self,QuestionID, QuestionNum,Question, MainQuestionNum, **kwargs):
        self.QuestionID = QuestionID
        self.QuestionNum = QuestionNum
        self.Question = Question
        self.VoiceStatus = kwargs["VoiceStatus"] if "VoiceStatus" in kwargs else None
        self.VoicePath = kwargs["VoicePath"] if "VoicePath" in kwargs else None
        # self.Tag = Tag
        self.MainQuestionNum = MainQuestionNum

#冠穎新增的
class FollowTags(Base):
    __tablename__ = "FollowTags"
    TagID = Column(Integer, primary_key=True)
    QuestionNum = Column(TEXT)
    Tag = Column(TEXT)

    def __init__(self, TagID, QuestionNum, Tag):
        self.TagID = TagID
        self.QuestionNum = QuestionNum
        self.Tag = Tag


def init_db():
    Base.metadata.create_all(DBLink)


def drop_db():
    Base.metadata.drop_all(DBLink)


init_db()


# 建db
# CREATE DATABASE i2aFor1111_Question CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
