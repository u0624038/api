# -*- coding: utf-8 -*-

import random
import os

import jieba
import jieba.analyse

import GenerateFollow_Q
import Update_GFQ
class Console(object):

    """
    Build some nlp function as an package.
    """

    def __init__(self, stopword="jieba_dict/stopword.txt",
                 jieba_dic="jieba_dict/dict.txt.big",
                 jieba_user_dic="jieba_dict/userdict.txt"):

        print("[Console] Building a console...")

        cur_dir = os.getcwd()
        curPath = os.path.dirname(__file__)
        os.chdir(curPath)
        
        # jieba custom setting.
        self.init_jieba(jieba_dic, jieba_user_dic)
        self.stopword = self.load_stopword(stopword)
        # build the rulebase.
        # self.rb = rulebase.RuleBase()

        # print("[Console] Loading the word embedding model...")

        # try:
        #     self.rb.load_model(model_path)
        # except FileNotFoundError as e:
        #     print("[Console] 請確定詞向量模型有正確配置")
        #     print(e)
        #     exit()
        # except Exception as e:
        #     print("[Gensim]")
        #     print(e)
        #     exit()
        print("[Console] Initialized successfully :>")
        os.chdir(cur_dir)

        

    # 載入追問題所有標簽
    def init_jieba(self, seg_dic, userdic):

        """
        jieba custom setting.
        """
        # 載入followQ 下的tag
        tag_status = GenerateFollow_Q.setFollow_Tag(userdic)
        if tag_status:
            jieba.load_userdict(userdic)
            jieba.set_dictionary(seg_dic)
            with open(userdic, 'r', encoding='utf-8') as input:
                for word in input:
                    word = word.strip('\n')
                    jieba.suggest_freq(word, True)


    # 去除不必要字詞
    def load_stopword(self, path):
        stopword = set()
        with open(path, 'r', encoding='utf-8') as stopword_list:
            for sw in stopword_list:
                sw = sw.strip('\n')
                stopword.add(sw)
        return stopword

    # 切詞
    def word_segment(self, sentence):
        S0 = sentence.replace(" ", "")
        S1 = S0.replace("(", "")
        S2 = S1.replace(")", "")
        words = jieba.cut(S2, HMM=False)
        #clean up the stopword
        keyword = []
        for word in words:
            if word not in self.stopword:
                if word not in keyword:
                    keyword.append(word)
        return keyword

    # Funtion QID 為職務or共通
    def rule_match(self, InterviewID, QID, Follow_word):
        user_keyword = self.word_segment(Follow_word)
        # 取得 qid 下的follow id
        Follow_QID = GenerateFollow_Q.getFollowQ_ID(QID)
        newItemID = "None"
        # 確認主題下是否有追問題而配對
        if Follow_QID == []:
            FQID = "None"
        else:
            FQID = GenerateFollow_Q.GenerateFollowQ_ID(Follow_QID, user_keyword)
        # 確認是否符合追問題條件而出題
        # try:
        if FQID == "None":
            return FQID, "None"
        else:
            newItemID = Update_GFQ.createFollow_Q(InterviewID, FQID)
            print("Create FollowQ Finished")
        # except:
            # print("ERROR Create FollowQ")
        # print(keyword)
        return FQID, newItemID
        

    
if __name__ == '__main__':
    # a = Console()
    # print(a.rule_match("123", "1001", "主管看電影管理看看商業內容"))
    main()
