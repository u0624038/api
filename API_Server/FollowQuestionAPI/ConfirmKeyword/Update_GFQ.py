import api_orm
import json

Session = api_orm.sessionmaker(bind=api_orm.DBLink)
session = Session()

def createFollow_Q(InterviewID, FQID):
    session = Session()
    InterviewLastID = session.query(api_orm.Item).\
        filter(api_orm.Item.InterviewID == InterviewID).\
        order_by(api_orm.Item.ItemID.desc()).first()
    newItemID = int(InterviewLastID.ItemID)+1
    print(newItemID)
    dt = api_orm.Item(ItemID=newItemID, InterviewID=InterviewID)
    session.add(dt)
    session.commit()
    session.close()
    linkItem_FollowQuestion(newItemID, FQID)
    return newItemID


def linkItem_FollowQuestion(ItemID, FQID):
    session = Session()
    newQuestion = api_orm.Question( 
            QuestionID=FQID, 
            Language="zh_tw", 
            Time=120, 
            ItemID=ItemID, 
            AudioURL=""
        )
    session.add(newQuestion)
    session.commit()
    session.close()
    
if __name__ == '__main__':
    createFollow_Q("28147", "1")