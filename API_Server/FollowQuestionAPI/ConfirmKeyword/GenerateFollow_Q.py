import Question_orm
import json

Session = Question_orm.sessionmaker(bind=Question_orm.DBLink)


def setFollow_Tag(userdic):
    session = Session()
    tag = []
    all_Tag = session.query(Question_orm.FollowTags).all()
    for i in all_Tag:
        if i.Tag not in tag:
            tag.append(i.Tag)
    session.commit()
    session.close()
    # print(tag)
    with open(userdic, 'w', encoding='utf-8') as f:
        for i in tag:
            f.write(i + "\n")
    print("TAG init Finished")
    return True

FollowQ = None
def getFollowQ_ID(QID):
    session = Session()
    QuestionID = int(QID)
    MainFollowQID = 0
    FollowQID = []
    #----------從qid 取得 positionNUM or GeneralNUM---------------------
    if QuestionID >= 1000:
        PQID = QuestionID - 1000
        PQ = session.query(Question_orm.PositionQuestion).\
            filter(Question_orm.PositionQuestion.QuestionID == PQID).first()
        MainFollowQID = PQ.QuestionNum
    else:
        GQID = QuestionID
        GQ = session.query(Question_orm.GeneralQuestion).\
            filter(Question_orm.GeneralQuestion.QuestionID == GQID).first()
        MainFollowQID = GQ.QuestionNum
    #----------從qid 取得 positionNUM or GeneralNUM---------------------
    #----------取得MainQ 下的FollowQNum----------------------------------
    global FollowQ
    FollowQ = session.query(Question_orm.FollowQuestion).\
            filter(Question_orm.FollowQuestion.MainQuestionNum == MainFollowQID).all()
    if FollowQ == None:
        session.close()
        return FollowQID
    else:
        for Q in FollowQ:
            FollowQID.append(Q.QuestionNum)
        session.close()
        return FollowQID

    #----------取得MainQ 下的FollowQNum---------------------------------

# 取得view的followqid並回傳
def getViewFID(FID):
    session = Session()
    FQ = session.query(Question_orm.FollowQuestion).\
        filter(Question_orm.FollowQuestion.QuestionNum == FID).first()
    
    FQ_View_ID = int(FQ.QuestionID) + 200000
    return FQ_View_ID


# 確認使用者詞是否配對追問題
def GenerateFollowQ_ID(Follow_QID, user_keyword):
    session = Session()
    for FID in Follow_QID:
        FollowQ_Tags = session.query(Question_orm.FollowTags).\
            filter(Question_orm.FollowTags.QuestionNum == FID).all()
        for tag in FollowQ_Tags:
            t = tag.Tag
            for word in user_keyword:
                if t == word:
                    # print(word, t, FID)
                    print("select FQ", FID)
                    # QuestionContentView
                    FolloQuestionID = getViewFID(FID)
                    return FolloQuestionID
                # else:
                #     # print(word, t, FID, "NONE")
                #     print(FID,"None select FQ")
    print("None select FQ")
    return "None"

if __name__ == "__main__":
    # setFollow_Tag("jieba_dict/userdict.txt")
    # setee()
    print(getFollowTag("1001"))