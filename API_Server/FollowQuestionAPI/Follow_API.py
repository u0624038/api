from flask import Flask, request, session, redirect, url_for, render_template, flash
import json
import ConfirmKeyword.console as ConfirmKey
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
host = config["SSL"]["host"]
port = config["SSL"]["port"]
# 載入切詞模組
Con_Key = ConfirmKey.Console()

app = Flask(__name__)
@app.route('/v2/Com/CreateFollowQuestion', methods=['POST'])
def CreateFollowQuestion():
    reqData = json.loads(bytes.decode(request.data))
    # print(reqData)
    # print(type(reqData))
    ItemID = reqData['ItemID']
    QID = reqData['QID']
    Follow_word = reqData['Follow_word']
    InterviewID = ItemID[:-2]
    print(InterviewID, QID)
    Return_QID, newItemID = Con_Key.rule_match(InterviewID, QID, Follow_word)

    data = {"QID": Return_QID, "ItemID":newItemID}
    # 回傳對話
    print(data)
    # print(json.dumps(data))
    return json.dumps(data)

@app.route('/v2/Com/init_jieba', methods=['POST'])
def init_jieba():
    reqData = json.loads(bytes.decode(request.data))
    print(reqData)
    print(type(reqData))
    num = reqData['jieba']
    if num == "1":
        global Con_Key
        Con_Key = ConfirmKey.Console()
    data = {"Status" : "Ok"}
    return json.dumps(data)
if __name__ == "__main__":
    app.run(host=host, port=port)
    # app.run(host="0.0.0.0", port=7777, debug=False, ssl_context=('./ssl/ca.crt', './ssl/private.key')) 
