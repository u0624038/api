# coding=UTF-8
import time,os,sys,json,requests,threading
from beacon import *
from EmotionCalculate import EmotionCalculate
import socket

# from demo_emotion import EmotionCalculate
def KeepAlive(Agent,Status,SessionID,InterviewID):
    global k
    while k:
        time.sleep(20)
        beacon(Agent,Status,SessionID,InterviewID)

def UpdateStatus(Agent,Status,SessionID):
    headers = {'Content-Type': 'application/json'}
    
    port='4003'
    router='v2/Process/UpdateEmotionProcess'
    foo={"Agent":Agent,"Status":Status,"SessionID":SessionID}
    r=requests.post('http://localhost:'+port+'/'+router,json=foo,headers=headers)

def UpdateResult(Result, SessionID, VlogResult):
    headers = {'Content-Type': 'application/json'}
    
    port='4003'
    router='v2/Process/UpdateEmotionAIResult'
    foo = {'SessionID':SessionID, 'EmotionInfo':Result, 'VlogResult':VlogResult}
    r=requests.post('http://localhost:'+port+'/'+router,json=foo,headers=headers)
    
    if r.status_code == requests.codes.ok:
      print("UpdateResultSuccess!")
    else:
      sleep(1)
      SplunkErrorLog(SessionID, r.status_code)
      UpdateResult(Result, SessionID, VlogResult)
      
def SplunkErrorLog(SessionID, status_code):
    foo = {'SessionID':SessionID, 'status_code':status_code}
    msg=foo
    serverAddressPort=('163.18.26.108',7602)
    UDPsocket=socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    UDPsocket.sendto(bytes(json.dumps(foo),"utf-8"),serverAddressPort)
    print("Splunk Error log OK!")
      
def SaveInSplunk(Result, SessionID, VlogResult,begin_time,over_time,ItemID):
    if VlogResult == 0:
        foo = {'SessionID':SessionID, 'EmotionInfo':'Error','VlogResult':VlogResult, 'APIName':'EmotionAPI', 'ItemID':ItemID}
    else:
        foo = {'SessionID':SessionID, 'EmotionInfo':Result, 'VlogResult':VlogResult,'Start':begin_time ,'Over':over_time, 'APIName':'EmotionAPI', 'ItemID':ItemID}
    msg=foo
    serverAddressPort=('163.18.26.108',7602)
    UDPsocket=socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    UDPsocket.sendto(bytes(json.dumps(foo),"utf-8"),serverAddressPort)
    print("Splunk OK!")
    


def main(SessionID, file_path, Orientation,ItemID):
    #load video and calculate emotion
    global outcome
    outcome = 0
    begin_time=""
    over_time=""
    error_flag = False
    try:
        r ,outcome ,begin_time ,over_time = EmotionCalculate(file_path, Orientation, error_flag)
        result = json.dumps(r)
    except Exception as e:
        result = '{"angry": "0.0", "disgust": "0.0", "fear": "0.0", "happy": "0.0", "sad": "0.0", "surprise": "0.0", "neutral": "0.0"}'
    #send result
    print('OK',result)
    UpdateResult(result, SessionID, outcome)
    SaveInSplunk(result, SessionID, outcome ,begin_time ,over_time,ItemID)
    if outcome == 1:
      print('T : %d' % outcome)
    else:
      print('F : %d' % outcome)
    #call beacon post agentMaster's Beacon (refresh)
    beacon("EmotionProcess","Processed",SessionID,0)
    #update database's Session process Status
    UpdateStatus("EmotionProcess","Processed",SessionID)
    #stop Beacon's thread
    k=0
    
if __name__ == '__main__':
    global k
    k = 1
    SessionID = sys.argv[1] 
    Orientation = int(sys.argv[2])
    ItemID = int(sys.argv[3])
    file_path = './files/'+SessionID+'.mkv'
    t = threading.Thread(target=KeepAlive, args=('EmotionProcess', 'Processing', SessionID,0))
    t.daemon = True
    t.start()
    main(SessionID, file_path, Orientation,ItemID)
