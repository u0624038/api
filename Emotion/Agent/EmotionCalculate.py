# coding=ISO-8859-1
from statistics import mode

import cv2,sys,time,os,sys,json,requests,threading
from keras.models import load_model
import numpy as np

from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.preprocessor import preprocess_input

# parameters for loading data and images
#    detection_model_path = './haarcascade_frontalface_default.xml'
base_dir = os.path.dirname(__file__)
#prototxt_path = os.path.join(base_dir + 'model_data/deploy.prototxt')
prototxt_path = ('./model_data/deploy.prototxt')

#caffemodel_path = os.path.join(base_dir + './model_data/weights.caffemodel')
caffemodel_path = ('./model_data/weights.caffemodel')
emotion_model_path = './fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')

# hyper-parameters for bounding boxes shape
frame_window = 10
emotion_offsets = (20, 40)

# loading models
model = cv2.dnn.readNetFromCaffe(prototxt_path, caffemodel_path)
emotion_classifier = load_model(emotion_model_path, compile=False)

# getting input model shapes for inference
emotion_target_size = emotion_classifier.input_shape[1:3]

# starting lists for calculating modes
p_value = [0]*7
output = {}

# starting Calculate
def EmotionCalculate(file_path, Orientation, error_flag):
    video_capture = cv2.VideoCapture(file_path)
    first_t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    count = 0
    count_1 = 0
    outcome = 0
    while True:
        ret, bgr_image = video_capture.read()
        # turn left 90 degrees and mirrored
        if(Orientation == 1):
            raw_frame = bgr_image
            foo = cv2.transpose(raw_frame)
            bgr_image = cv2.flip(foo, 0)
        # mirrored
        elif(Orientation == 2):
            raw_frame = bgr_image
            bgr_image = cv2.flip(raw_frame, 0)
        # turn left 90 degrees
        elif(Orientation == 3):
            raw_frame = bgr_image
            bgr_image = cv2.transpose(raw_frame)
            
        timestamp = int(video_capture.get(cv2.CAP_PROP_POS_MSEC)/1000)
        #print(timestamp)
        if timestamp == count:
            (h, w) = bgr_image.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(bgr_image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
            model.setInput(blob)
            detections = model.forward()
            for i in range(0, detections.shape[2]):
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                confidence = detections[0, 0, i, 2]
                
                if (confidence > 0.5):

                    outcome = 1
                    
                    frame = bgr_image[startY:endY, startX:endX]
                    
                    try:
                        gray_face = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    except:
                        break
                    '''x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
                    gray_face = gray_image[y1:y2, x1:x2]'''
                    try:
                        gray_face = cv2.resize(gray_face, (emotion_target_size))
                    except:
                        continue
                    

                    gray_face = preprocess_input(gray_face, True)
                    gray_face = np.expand_dims(gray_face, 0)
                    gray_face = np.expand_dims(gray_face, -1)
                    emotion_prediction = emotion_classifier.predict(gray_face)
                    for i,value in enumerate(emotion_prediction[0]):
                        p_value[i] += value
                    emotion_probability = np.max(emotion_prediction)
                    emotion_label_arg = np.argmax(emotion_prediction)
                    emotion_text = emotion_labels[emotion_label_arg]
                    end_t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                    s_t=str(first_t)
                    e_t=str(end_t)

                    #total = end_t-first_t
                    count_1 += 1
            count += 1
        elif ret == False:
            for i,value in enumerate(p_value):
                try:
                    p_value[i] = round(value/(count_1), 2)
                    foo = str(p_value[i])
                    output.update({emotion_labels[i]:foo})                    
                except ZeroDivisionError:
                    if error_flag == False:
                        print("error")
                        error_flag = True
                        end_t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                        s_t=str(first_t)
                        e_t=str(end_t)
                        output1, outcome1 ,s_t , e_t= EmotionCalculate(file_path, 3, error_flag)
                        video_capture.release()
                        return output1, outcome1 ,s_t , e_t
                    else:
                        return -1
            video_capture.release()
            break
        else:
            continue
                    
    print(output)
    return output ,outcome ,s_t ,e_t


if __name__ == '__main__':
    output = EmotionCalculate('convert2.mkv', 1, False)