# coding=UTF-8
from flask import Flask, session, redirect, url_for,request
import time,json
import subprocess
import threading
import requests
import os
app = Flask(__name__)
import socket
import configparser

config = configparser.ConfigParser()
config.read('set.conf')
host = config["AM"]["host"]

#-------------Call Agents-----------------
def main():
    #-------- Agents name-----------------
    headers = {'Content-Type': 'application/json'}
    #Agents=['NaNaProcess','AudioProcess','EmotionProcess','UploadProcess','GenerateProcess']
    #Routes=['SearchNaNaProcess','SearchAudioProcess','SearchEmotionProcess','SearchUploadProcess','SearchGenerateProcess']
    Agents=['EmotionProcess']
    Routes=['SearchEmotionProcess']
    Port=['4003']
    #Port=['1013','1013','1998','4003','1825']
    while 1:#1=true
        time.sleep(5)
        for i in range(0,len(Agents)):
            time.sleep(2)
            try:
                r = requests.post('http://localhost:4003/v2/Session/'+Routes[i],headers=headers)
                print(r.text)
                if r.text !="None":
                    data = json.loads(r.text)
                    if Agents[i]=='GenerateProcess':
                        InterviewID=str(data["InterviewID"])
                        SessionID=''
                    else:
                        SessionID = str(data["SessionID"])
                        InterviewID='0'
                    time.sleep(2)
                    if Agents[i]=='UploadProcess':
                        Proc = subprocess.Popen(['sudo','python3','UploadProcess.py','--SessionID='+SessionID,'--InterviewID='+InterviewID, '--file=./files/'+SessionID+".mkv",'--title='+SessionID])
                    elif Agents[i]=='Generate':
                        a={
                        'InterviewID':InterviewID,
                        'Profile':data["Profile"],
                        'SessionID':SessionID
                        }
                        b=json.dumps(a)
                        Proc = subprocess.Popen(['sudo','python3','Generate.py',b])
                    else:
                        Orientation=str(data["Orientation"])
                        print(Orientation)
                        ItemID = str(data["ItemID"])
                        print(ItemID)
                        print(type(Orientation))
                        foo={"Agent":Agents[i],"Status":"Processing","SessionID":SessionID}
                        r_up=requests.post('http://localhost:'+Port[i]+"/v2/Process/Update"+Agents[i],data=json.dumps(foo),headers=headers)
                        Proc = subprocess.Popen(['sudo','python3',Agents[i]+'.py',SessionID,Orientation,ItemID])
                    print('--------------DO : '+Agents[i]+'-----------------')
                    PID =Proc.pid #拿到呼叫起來的副程式PID proc.pid 進程ID
                    #將PID Post到API 存到資料庫 (確認副程式存活狀況用)
                    Time_ =time.strftime('%I%M%S',time.localtime(time.time())) #%I 12小時制 M分鐘 S秒數
                    info={"SessionID":SessionID,"InterviewID":InterviewID,"Time":Time_,"PID":PID,"Host":host,"Agent":Agents[i]}
                    r_init=requests.post('http://localhost:4003/v2/Process/InitProcess',data=json.dumps(info),headers=headers)#post到api 存狀況進入ProcessCheck這張表
                    time.sleep(2)
                    foo={"Agent":Agents[i],"Status":"Processing","SessionID":SessionID}
                    r_up=requests.post('http://localhost:'+Port[i]+"/v2/Process/Update"+Agents[i],data=json.dumps(foo),headers=headers)
                    time.sleep(2)
                else:#等於none的話就是處理好了 所以pass
                    print('Pass')
                    pass
            except Exception as e:

                print(e)
                pass
#--------------Check Timeout-----------------
#每十秒確認跟APIserver 程式存活狀況
def CheckAlive ():
    headers = {'Content-Type': 'application/json'}
    while 1:
        time.sleep(20)
        try:
            print('try check')
            #POST API 確認副程式存活
            a={'Host':host}
            r_cp=requests.post('http://localhost:4003/v2/Process/CheckSubProcess',data=json.dumps(a),headers=headers)
            if r_cp.text !="None":#不等於None的話會回傳pid和time
                data=json.loads(r_cp.text)
                #抓當前時間
                now_time=time.strftime('%I%M%S',time.localtime(time.time()))
                now_time=int(now_time)
                #拿資料庫拿到的時間
                time_=int(data["Time"])
                #相減>100 的話把副程式殺掉 (拿PID殺)
                print('db time : ' + str(data["Time"]))
                print('now time: ' + str(now_time))
                #print('now_time-db_time: '+ str(now_time-time_))
                if (now_time-time_) >100:
                    PID=str(data["PID"])
                    #linux kill -9
                    #winodws taskill
                    os.system("sudo kill -9"+PID)# 殺程序只能用pid來殺
                    foo = {"PID":PID,"Host":host}
                    print('killprocess')
                    r_kp=requests.post('http://localhost:4003/v2/Process/KillProcess',data=json.dumps(foo),headers=headers)
                    #post api去把pid從表內刪除
                else :#相減時間<=100
                    pass
            else:#r_cp.text==None
                pass
        except Exception as e:
            
            print("123",e)
            pass
#------------------------Beacon---------------------------
#接收呼叫起來的副程式每10秒傳Keepalive過來  NaNa.py呼叫beacon.py beacon.py再post到這 確認nana是否存活
@app.route('/Beacon',methods=['POST'])
def Beacon():
    print(request.data)
    headers = {'Content-Type': 'application/json'}
    Data_Dict = json.loads(bytes.decode(request.data))
    Agent = Data_Dict["Agent"]
    Status = Data_Dict["Status"]
    Time_ = Data_Dict["Time"]
    SessionID = Data_Dict["SessionID"]
    InterviewID = Data_Dict["InterviewID"]
    data={
    "Agent":Agent,
    "Status":Status,
    "Time":Time_,
    "SessionID":SessionID,
    "InterviewID":InterviewID
    }
    r_up_bc = requests.post('http://localhost:4003/v2/Process/UpdateProcess',data=json.dumps(data),headers=headers)#post更新進程狀態
    return(json.dumps(Data_Dict))
@app.route('/UploadFiles',methods=['POST'])
def UploadFiles():
    #SessionID = str(MetaData['SessionID'])
    File = request.files['File']
    path='./files/'+File.filename
    print(path)
    File.save(os.path.join(path))
    return ("ok")
if __name__ == '__main__':
    #-----------------------
    Call_Agent_Thread=threading.Thread(target=main)
    Call_Agent_Thread.daemon = True
    Call_Agent_Thread.start()
    #-----------------------
    Check_Agent_Thread= threading.Thread(target=CheckAlive)
    Check_Agent_Thread.daemon = True
    Check_Agent_Thread.start()
    #-----------------------
    app.run(host='0.0.0.0',port=6000,debug=True)
