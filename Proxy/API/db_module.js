var Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const config = require('./config.js');
var MasterDB_IP = config.DB_Master_IP
var MasterDB_Name = config.DB_Master_Name
var MasterDB_Account = config.DB_Master_Account
var MasterDB_Password = config.DB_Master_Password

var sequelize = new Sequelize( MasterDB_Name, MasterDB_Account, MasterDB_Password, {
  host: MasterDB_IP,
  dialect: 'mysql',
  pool: {
    max: 150,
    min: 0,
    acquire:90000,
    idle: 10000
  }
});

var Session = sequelize.define('Session', {
  SessionID: {type:Sequelize.STRING,primaryKey: true,allowNull: false},
  AVPath:{type:Sequelize.STRING, defaultValue: ''},
  VTTURL:{type:Sequelize.STRING, defaultValue: ''},
  VideoURL:{type:Sequelize.STRING, defaultValue: ''},
  ItemID:{type:Sequelize.INTEGER},
  InterviewID:Sequelize.INTEGER,
  Orientation:{type:Sequelize.INTEGER, defaultValue :0},
},{timestamps: true,freezeTableName: true,});


var Proxy = sequelize.define('Proxy', {
  id: {type:Sequelize.INTEGER,primaryKey: true, allowNull: false, autoIncrement: true,},
  files:{type:Sequelize.STRING, },
  upload:{type:Sequelize.INTEGER, },
  DataSize:{type:Sequelize.STRING, },
  SessionID:{type:Sequelize.STRING, },
  conver:{type:Sequelize.INTEGER, },
},{timestamps: true,freezeTableName: true,});

module.exports = {Proxy,Session}

