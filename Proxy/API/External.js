const config = require('./config.js');
var express = require('express');
// 建立 express 例項
var db = require('./db_module');
const app = express();
var fs = require('fs');
const path = require('path');
var uuidv1 = require('uuid/v1');
const bodyParser = require('body-parser');
const multer = require('multer');
const https = require('https');
var UploadfilePath = config.UploadfilePath
var video_File_Extension = config.Uploadfile_File_Extension
var privateKey = fs.readFileSync(config.SSL_PrivateKey, 'utf8');
var certificate = fs.readFileSync(config.SSL_Certificate, 'utf8');
var ca = fs.readFileSync(config.SSL_Ca, 'utf8');
var credentials = { key: privateKey, cert: certificate, ca: ca };
var jsonfind = require('json-find');


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, UploadfilePath);  // 儲存的路徑，備註：需要自己建立
  },
  filename: function (req, file, cb) {
    // 將儲存檔名設定
    cb(null, uuidv1() + video_File_Extension);
  }
});

var upload = multer({ storage: storage })


app.use(bodyParser.json({ limit: '1mb' }));  //body-parser 解析json格式数据
app.use(bodyParser.urlencoded({            //此项必须在 bodyParser.json 下面,为参数编码
  extended: true
}));

let { PythonShell } = require('python-shell')
var error_str = ''

process.on('exit', function (code, err) {
  console.log(error_str)
  let option = {
    args:
      [
        error_str.stack
      ]
  }
  PythonShell.run('test_server_stop_email.py', option, function (err, data) {
    if (err) throw err;
    console.log(data)
    console.log('finished');
    //console.log('server close')
  });
  console.log('server close');
});



function isDigit(s) {
  var patrn = /^[0-9]{1,20}$/;
  if (!patrn.exec(s)) {
    return false
  }
  return true
}

function isUUID(s) {
  var patrn = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/;
  if (!patrn.exec(s)) {
    return false
  }
  return true
}

process.on('uncaughtException', function (err) {
  console.log(err);
  error_str = err
  try {
    var killTimer = setTimeout(function () {
      //process.exit(1);
    }, 30000);
    killTimer.unref();
    //server.close();
  } catch (e) {
    console.log('error when exit', e.stack);
  }
});


app.get('/', (req, res) => {
  console.log(req.headers);

  /*process.on('uncaughtException', function (err) {
    console.log(err.message);
    var error = [err.toString()]
    console.log(error)
    let option = {
      args:
        [
          err.stack
        ]
  }
  PythonShell.run('test_server_stop_email.py', option, function (err,data) {
    if (err) throw err;
    console.log(data)
    console.log('finished');
    //console.log('server close')
  });

    try {
        var killTimer = setTimeout(function () {
            //process.exit(1);
        }, 10000);
        killTimer.unref();
        //server.close();
    } catch (e) {
        console.log('error when exit', e.stack);
    }
  });*/
  setTimeout(function () {
    // throws Error
    throw new Error('async exception');
  }, 1000);

  return res.send('ok');
})

app.post('/v2/Session/RequestJobID', (req, res) => {
  db.Proxy.count().then(function (c) {
    console.log(c)

    db.Proxy.create({
      //    id:c,
      files: '',
      upload: 0,
      DataSize: '',
      SessionID: '',
      conver: 0,
    }).then(function (result) {
      console.log("Create Jobid");
      //console.log(result.JobID);
      var str = { JobID: result.id }
      return res.send(str);
    }).catch(function (err) {
      console.log(err.message);
    });

  });
});



app.post('/v2/Session/UploadFiles', upload.single('File'), (req, res) => {

  process.on('uncaughtException', function (err) {
    console.log(err);
    try {
      var killTimer = setTimeout(function () {
        //process.exit(1);
      }, 30000);
      killTimer.unref();
      //server.close();
    } catch (e) {
      console.log('error when exit', e.stack);
    }
  });

  var file = req.file;
  console.log('名稱：%s', file.originalname);
  //console.log('檔案名稱：%s', file.filename);
  //console.log('檔案大小：%s', file.size);
  //console.log('檔案路徑：%s', file.path);

  var jobid = req.headers.jobid;
  var datasize = req.headers.datasize
  var sessionid = req.headers.sessionid;
  //var metadata = {
  //  JobID:jobid,
  //  SessionID:sessionid,
  //  DataSize:file.size,
  //  Path:'./uploadfile/'+(sessionid+'.mkv')
  //}
  console.log('originalname: %s', req.file.originalname.split('.')[1])
  var original = req.file.originalname.split('.')
  console.log(original)
  if (original.length == 1) {
    return res.send({ Status: 'File has not File extension' })
  } else {
    if (req.file.originalname.split('.')[1] != 'mp4') {
      return res.send({ Status: 'file format is not mp4' })
    } else {
      if (file.size == 1) {
        return res.send({ Status: 'upload empty file' })
      } else {
        if (jsonfind(req.headers).checkKey('sessionid') == false || jsonfind(req.headers).checkKey('jobid') == false || jsonfind(req.headers).checkKey('datasize') == false) {
          return res.send({ Status: 'no key or JobID and DataSize must > 0' })
        } else {
          if (isDigit(datasize.toString()) && isDigit(jobid.toString())) {
            if (isUUID(sessionid.toString())) {
              db.Session.findAll({
                where: {
                  SessionID: sessionid
                }
              }).then(function (result) {
                if (result.length >= 1) {
                  result[0].update({
                    Orientation: 1,
                  }).catch(function (err) {
                    console.log(err.message)
                  });
                } else {
                  return res.send({ Status: 'can not find this SessionID' })
                }
              }).catch(function (err) {

                return res.send({ Status: err.message })
              });


              //metadata = JSON.stringify(metadata)
              //console.log(typeof(metadata_json))
              //console.log(file);
              //console.log(sessionid);
              console.log(jobid);
              fs.rename(('./files/' + file.filename), ('./files/' + (sessionid + '.mp4')), function (err) {
                if (err) throw err;

                db.Proxy.findAll({
                  where: {
                    id: jobid
                  }
                }).then(function (result) {
                  if (result.length >= 1) {
                    result[0].update({
                      SessionID: sessionid,
                      DataSize: datasize,
                      files: ('./files/' + (sessionid + '.mp4'))
                    }).then(function (result1) {
                      console.log("Uploa-d Sucess");

                      var str = { Status: "ok" }
                      return res.send(str);
                    }).catch(function (err) {
                      console.log(err.message);
                      return res.send({ Status: err.message })
                    });
                  } else {
                    return res.send({ Status: 'can not find this JobID' })
                  }
                }).catch(function (err) {
                  console.log(err.message);
                  return res.send({ Status: err.message })
                });
                //console.log(createjob.create_jobid(jobid,metadata,('./files/'+sessionid+'.mkv')))

              });

            } else {
              return res.send({ Status: 'error SessionID is not correct' })
            }
          } else {
            return res.send({ Status: 'error ItemID or Job is not a int' })
          }
        }
      }
    }
  }
});



app.post('/v2/Session/CheckFiles', (req, res) => {
  //var id = req.headers.jobid;
  //console.log(req.headers);
  var id = req.headers.jobid;
  console.log(id)
  console.log(jsonfind(req.headers).checkKey('jobid'))
  if (jsonfind(req.headers).checkKey('jobid') == false) {
    return res.send({ Result: 'no key' })
  } else {
    if (isDigit(id.toString())) {
      db.Proxy.findAll({
        where: {
          id: id
        }
      }).then(function (result) {
        console.log('query all users');
        //console.log(result.length)
        if (result.length >= 1) {
          var path = result[0].files;

          //var MetaData = JSON.parse(result[0].MetaData);
          //var sessionid = MetaData.SessionID;
          var db_size = result[0].DataSize;
          console.log(db_size)
          console.log(path)
          if (path.length > 0) {
            fs.stat(path, (err, stats) => {
              if (err) throw err;
              console.log(stats.size)
              if (stats.size == db_size) {
                return res.send({ Result: true });
              }
              else {
                return res.send({ Result: false });
              }
            });
          } else {
            return res.send({ Result: false });
          }
        }
      }).catch(function (err) {
        console.log(err.message);
        return res.send({ Result: err.message });
      });
    } else {
      return res.send({ Result: 'Job is not a int' });
    }
  }
});

const httpsServer = https.createServer(credentials, app);

httpsServer.listen(4000, '0.0.0.0', () => {
  console.log('HTTPS Server running on port 4000');

});


//app.listen(3004, () =>
//  console.log('Example app listening on port 3004!')

//);
